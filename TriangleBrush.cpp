//
// TriangleBrush.cpp
//
// The implementation of Triangle Brush. 
// 
//

#include "impressionistDoc.h"
#include "impressionistUI.h"
#include "TriangleBrush.h"
#include <cmath>

extern float frand();

TriangleBrush::TriangleBrush(ImpressionistDoc* pDoc, char* name) :
ImpBrush(pDoc, name)
{
}

void TriangleBrush::BrushBegin(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* dlg = pDoc->m_pUI;

	glPointSize(1.f);

	BrushMove(source, target);
}

void TriangleBrush::BrushMove(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* dlg = pDoc->m_pUI;

	if (pDoc == NULL) {
		printf("TriangleBrush::BrushMove  document is NULL\n");
		return;
	}

	Point minPt = MinPoint();
	Point maxPt = MaxPoint();
	if (!ValidPointi(target, Point())) {
		// allow start dragging from out of boundary

		//printf("Out of Boundary\n");
		//return;
	}

	int size = pDoc->getSize()+2;

	Pointf upPt(target.x, target.y + size*sin(PI/3)/2);
	Point leftPt(target.x - size*cos(PI/3), target.y - size*sin(PI/3)/2);
	Point rightPt(target.x + size*cos(PI/3), target.y - size*sin(PI/3)/2);

	glBegin(GL_POLYGON);
	SetColor(source);

	glVertex2f(upPt.x, upPt.y);
	glVertex2f(leftPt.x, leftPt.y);
	glVertex2f(rightPt.x, rightPt.y);

	glEnd();
}

void TriangleBrush::BrushEnd(const Point source, const Point target)
{
	// do nothing so far
}


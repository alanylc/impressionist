//
// ScatteredLineBrush.cpp
//
// The implementation of Scattered Line Brush. 
// 
//

#include "impressionistDoc.h"
#include "impressionistUI.h"
#include "ScatteredLineBrush.h"
#include <cmath>

extern float frand();
extern int irand(int imax);

ScatteredLineBrush::ScatteredLineBrush(ImpressionistDoc* pDoc, char* name) :
ImpBrush(pDoc, name)
{
}

void ScatteredLineBrush::BrushBegin(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* dlg = pDoc->m_pUI;

	int lineWidth = pDoc->getLineWidth();
	glLineWidth((float)lineWidth);

	prevPt = Point(target.x, target.y);

	BrushMove(source, target);
}

void ScatteredLineBrush::BrushMove(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* dlg = pDoc->m_pUI;

	if (pDoc == NULL) {
		printf("ScatteredLineBrush::BrushMove  document is NULL\n");
		return;
	}

	Point minPt = MinPoint();
	Point maxPt = MaxPoint();
	if (!ValidPointi(target, Point())) {
		// allow start dragging from out of boundary

		//printf("Out of Boundary\n");
		//return;
	}

	int size = pDoc->getSize();

	if (size < 2) {
		return;
	}

	float angle = 0.f;
	int dx = target.x - prevPt.x;
	int dy = target.y - prevPt.y;

	switch (pDoc->getStrokeDirection()) {
		case SLIDERMOUSE:
			angle = pDoc->getAngle() * PI / 180; // in radian
			break;
		case GRADIENT:
			angle = pDoc->m_pUI->m_origView->getGradient(source);
			break;
		case BRUSHDIRECTION:
			if (abs(dx) > 1 || abs(dy) > 1) {
				angle = atan2(dy, dx); // in radian
				if (angle < EPSILON) angle += 2 * PI;
			}
			else {
				return;
			}
			break;
		default:
			break;
	}

	int lineCount = irand(2) + 3; // minimum 3 lines, maximum 5 lines

	for (int i = 0; i < lineCount; i++) {

		int xShift = irand((int)size*0.8) - (int)size*0.8/2 + 1;
		int yShift = irand((int)size*0.8) - (int)size*0.8/2 + 1;
		Point pt(target.x+xShift, target.y+yShift);
		ValidPointi(pt, pt);


		Pointf leftPt, rightPt;
		computeLinePoints(angle, Pointf((float)pt.x, (float)pt.y), size, leftPt, rightPt);

		Point shifted_source = Point(source.x + (pt.x - target.x), source.y + (pt.y - target.y));

		glBegin(GL_LINE_STRIP);
		SetColor(shifted_source);

		glVertex2d(leftPt.x, leftPt.y);
		glVertex2d(rightPt.x, rightPt.y);

		glEnd();
	}

	prevPt = Point(target.x, target.y);
}

void ScatteredLineBrush::BrushEnd(const Point source, const Point target)
{
	// do nothing so far
}


//
// paintview.cpp
//
// The code maintaining the painting view of the input images
//

#include "impressionist.h"
#include "impressionistDoc.h"
#include "impressionistUI.h"
#include "paintview.h"
#include "ImpBrush.h"
#include "BlurBrush.h"
#include <cmath>


#define LEFT_MOUSE_DOWN		1
#define LEFT_MOUSE_DRAG		2
#define LEFT_MOUSE_UP		3
#define RIGHT_MOUSE_DOWN	4
#define RIGHT_MOUSE_DRAG	5
#define RIGHT_MOUSE_UP		6


#ifndef WIN32
#define min(a, b)	( ( (a)<(b) ) ? (a) : (b) )
#define max(a, b)	( ( (a)>(b) ) ? (a) : (b) )
#endif

extern int irand(int max);

static int		eventToDo;
static int		isAnEvent=0;

static Point	coord(0,0);
static bool		allowMouseStrokeDirection = false; // Point Brush as Default brush

// Coord for right drag, stroke direction
static Point	startPt, endPt;

PaintView::PaintView(int			x, 
					 int			y, 
					 int			w, 
					 int			h, 
					 const char*	l)
						: Fl_Gl_Window(x,y,w,h,l)
{
	m_nWindowWidth	= w;
	m_nWindowHeight	= h;
	doPaint = false;

}

void PaintView::draw()
{
	#ifndef MESA
	// To avoid flicker on some machines.
	glDrawBuffer(GL_FRONT_AND_BACK);
	#endif // !MESA

	if(!valid())
	{

		glClearColor(0.7f, 0.7f, 0.7f, 1.0);

		// We're only using 2-D, so turn off depth 
		glDisable( GL_DEPTH_TEST );

		ortho();

		glClear( GL_COLOR_BUFFER_BIT );

		// enable alpha operation
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	}

	Point scrollpos;// = GetScrollPosition();
	scrollpos.x = 0;
	scrollpos.y	= 0;

	m_nWindowWidth	= w();
	m_nWindowHeight	= h();

	int drawWidth, drawHeight;
	drawWidth = min( m_nWindowWidth, m_pDoc->m_nPaintWidth );
	drawHeight = min( m_nWindowHeight, m_pDoc->m_nPaintHeight );

	int startrow = m_pDoc->m_nPaintHeight - (scrollpos.y + drawHeight);
	if ( startrow < 0 ) startrow = 0;

	m_pPaintBitstart = m_pDoc->m_ucPainting + 
		3 * ((m_pDoc->m_nPaintWidth * startrow) + scrollpos.x);

	m_nDrawWidth	= drawWidth;
	m_nDrawHeight	= drawHeight;

	m_nStartRow		= startrow;
	m_nEndRow		= startrow + drawHeight;
	m_nStartCol		= scrollpos.x;
	m_nEndCol		= m_nStartCol + drawWidth;


	if (m_pDoc->m_ucPainting && doPaint)
	{
		autoPaint();
		doPaint = false;
		SaveCurrentContent();
	}

	if ( m_pDoc->m_ucPainting && !isAnEvent) 
	{
		RestoreContent();
	}
	

	if ( m_pDoc->m_ucPainting && isAnEvent) 
	{

		// Clear it after processing.
		isAnEvent	= 0;	

		Point source( coord.x + m_nStartCol, m_nEndRow - coord.y );
		Point target( coord.x, m_nWindowHeight - coord.y );
		
		Point minPt = m_pDoc->m_pCurrentBrush->MinPoint();

		// This is the event handler
		switch (eventToDo) 
		{
		case LEFT_MOUSE_DOWN:
			glScissor(minPt.x, minPt.y, drawWidth, drawHeight);
			glEnable(GL_SCISSOR_TEST);
			SaveCurrentContent();
			RestoreContent();
			m_pDoc->m_pCurrentBrush->BrushBegin( source, target );
			break;
		case LEFT_MOUSE_DRAG:
			m_pDoc->m_pCurrentBrush->BrushMove( source, target );
			break;
		case LEFT_MOUSE_UP:
			m_pDoc->m_pCurrentBrush->BrushEnd( source, target );
			SaveCurrentContent();
			RestoreContent();
			glDisable(GL_SCISSOR_TEST);
			break;
		case RIGHT_MOUSE_DOWN:
			startPt = Point(target.x, target.y);
			glLineWidth(1.f);
			break;
		case RIGHT_MOUSE_DRAG:
			endPt = Point(target.x, target.y);
			// draw the red line and remove it when mouse up
			RestoreContent();
			glBegin(GL_LINE_STRIP);
			glColor3f(1.f, 0.f, 0.f);
			glVertex2i(startPt.x, startPt.y);
			glVertex2i(endPt.x, endPt.y);
			glEnd();
			break;
		case RIGHT_MOUSE_UP:
			RestoreContent();
			RestoreFront();
			if (m_pDoc->m_nStrokeDirection == SLIDERMOUSE && allowMouseStrokeDirection) {
				int angle = (int)( atan2(endPt.y-startPt.y, endPt.x-startPt.x) * 180 / PI ); // in degree
				if (angle < 0) angle += 360;
				m_pDoc->m_pUI->setAngle(angle);
			}
			break;

		default:
			printf("Unknown event!!\n");		
			break;
		}
	}

	glFlush();

	#ifndef MESA
	// To avoid flicker on some machines.
	glDrawBuffer(GL_BACK);
	#endif // !MESA

}


int PaintView::handle(int event)
{
	switch (event)
	{
	case FL_ENTER:
		redraw();
		break;
	case FL_PUSH:
		coord.x = Fl::event_x();
		coord.y = Fl::event_y();
		if (Fl::event_button() > 1)
			eventToDo = RIGHT_MOUSE_DOWN;
		else
			eventToDo = LEFT_MOUSE_DOWN;
		isAnEvent = 1;
		redraw();
		break;
	case FL_DRAG:
		coord.x = Fl::event_x();
		coord.y = Fl::event_y();
		if (Fl::event_button() > 1)
			eventToDo = RIGHT_MOUSE_DRAG;
		else
			eventToDo = LEFT_MOUSE_DRAG;
		isAnEvent = 1;
		redraw();
		break;
	case FL_RELEASE:
		coord.x = Fl::event_x();
		coord.y = Fl::event_y();
		if (Fl::event_button() > 1)
			eventToDo = RIGHT_MOUSE_UP;
		else
			eventToDo = LEFT_MOUSE_UP;
		isAnEvent = 1;
		redraw();
		break;
	case FL_MOVE:
		coord.x = Fl::event_x();
		coord.y = Fl::event_y();
		break;
	default:
		return 0;
		break;
	}

	Point vpt(coord.x, m_nWindowHeight - coord.y);
	m_pDoc->m_pCurrentBrush->ValidPointi(vpt, vpt);


	m_pDoc->m_pUI->m_origView->setCoord(vpt.x, vpt.y);
	m_pDoc->m_pUI->m_origView->refresh();

	return 1;
}

void PaintView::refresh()
{
	redraw();
}

void PaintView::resizeWindow(int width, int height)
{
	resize(x(), y(), width, height);
}

void PaintView::undo()
{
	if (m_pDoc->m_ucPainting == NULL || m_pDoc->m_ucUndo == NULL) return;
	memcpy(m_pDoc->m_ucPainting, m_pDoc->m_ucUndo, (size_t)m_nDrawHeight*m_nDrawWidth* 3);
	RestoreContent();
	RestoreFront();
	SaveCurrentContent();
	refresh();
	m_pDoc->m_pUI->m_origView->refresh();
}

void PaintView::SaveCurrentContent()
{
	// save prev pic
	memcpy(m_pDoc->m_ucUndo, m_pDoc->m_ucPainting, (size_t)m_nDrawHeight*m_nDrawWidth * 3);


	// Tell openGL to read from the front buffer when capturing
	// out paint strokes
	glReadBuffer(GL_FRONT);

	glPixelStorei( GL_PACK_ALIGNMENT, 1 );
	glPixelStorei( GL_PACK_ROW_LENGTH, m_pDoc->m_nPaintWidth );
	
	glReadPixels( 0, 
				  m_nWindowHeight - m_nDrawHeight, 
				  m_nDrawWidth, 
				  m_nDrawHeight, 
				  GL_RGB, 
				  GL_UNSIGNED_BYTE, 
				  m_pPaintBitstart );
}


void PaintView::RestoreContent()
{
	glDrawBuffer(GL_BACK);

	glClear( GL_COLOR_BUFFER_BIT );

	glRasterPos2i( 0, m_nWindowHeight - m_nDrawHeight );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );
	glPixelStorei( GL_UNPACK_ROW_LENGTH, m_pDoc->m_nPaintWidth );
	glDrawPixels( m_nDrawWidth, 
				  m_nDrawHeight, 
				  GL_RGB, 
				  GL_UNSIGNED_BYTE, 
				  m_pPaintBitstart);
}

void PaintView::RestoreFront()
{
	glDrawBuffer(GL_FRONT);

	glClear(GL_COLOR_BUFFER_BIT);

	glRasterPos2i(0, m_nWindowHeight - m_nDrawHeight);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glPixelStorei(GL_UNPACK_ROW_LENGTH, m_pDoc->m_nPaintWidth);
	glDrawPixels(m_nDrawWidth,
		m_nDrawHeight,
		GL_RGB,
		GL_UNSIGNED_BYTE,
		m_pPaintBitstart);
}

void PaintView::setStroke(bool turnOn)
{
	allowMouseStrokeDirection = turnOn;
}

void PaintView::paintAll()
{
	doPaint = true;
	// set the firstTime compute blur flag to be true (turned off in blur brush - brushBegin function)
	((BlurBrush*)(ImpBrush::c_pBrushes[BRUSH_BLUR]))->firstTime = true;
	refresh();
}

void PaintView::randSize(int orgSize)
{
	int rand = 0;
	int imax = (int)orgSize * 0.7;

	// in case of imax == 0, rand = 0
	if (imax == 1) {
		rand = irand(2);
	}
	else if (imax > 1) {
		rand = irand(imax);
	}
	int signRand = irand(200);
	if (signRand < 100) {
		rand = -rand;
	}
	else {
		rand = (int) rand * 0.2;
	}
	int rsize = orgSize + rand;
	m_pDoc->m_pUI->setSize(rsize, false);
}

void PaintView::autoPaint()
{
	int rand;
	int org_size = m_pDoc->getSize();
	int spacing = m_pDoc->m_pUI->getSpacing();
	if (spacing == 1) spacing++;
	bool sizeRand = m_pDoc->m_pUI->getSizeRand();
	Point minPt = m_pDoc->m_pCurrentBrush->MinPoint();

	int pointcount = 0;
	Point* points = new Point[m_pDoc->m_nPaintWidth*m_pDoc->m_nPaintHeight];

	// compute points to paint
	for (int i = 0; i < m_pDoc->m_nPaintWidth; i += spacing) {
		for (int j = 0; j < m_pDoc->m_nPaintHeight; j += spacing) {
			points[pointcount] = Point(i, j);
			pointcount++;
		}
	}

	// swap the points so that paint in random order
	for (int i = 0; i < m_pDoc->m_nPaintWidth*m_pDoc->m_nPaintHeight * 4; i++) {
		int a = irand(pointcount);
		int b = irand(pointcount);
		Point tmp(points[a].x, points[a].y);
		points[a] = Point(points[b].x, points[b].y);
		points[b] = Point(tmp.x, tmp.y);
	}

	// paint the points
	for (int i = 0; i < pointcount; i++) {
		Point pos = points[i];
		Point source(pos.x + m_nStartCol, m_nEndRow - pos.y);
		Point target(pos.x, m_nWindowHeight - pos.y);
		if (sizeRand)	randSize(org_size);
		m_pDoc->m_pCurrentBrush->BrushBegin(source, target);
		m_pDoc->m_pCurrentBrush->BrushEnd(source, target);
	}
	m_pDoc->m_pUI->setSize(org_size, true);
}

void PaintView::scaleColor(float r, float g, float b)
{
	if (m_pDoc->m_ucPainting == NULL) return;
	GLubyte color[3];
	for (int i = 0; i < m_nDrawHeight*m_nDrawWidth * 3; i+=3){
		memcpy(color, (GLubyte*)m_pPaintBitstart+i, 3);
		float fcolor[3];
		for (int j = 0; j < 3; j++) fcolor[j] = (float) color[j];
		fcolor[0] *= r;
		fcolor[1] *= g;
		fcolor[2] *= b;
		for (int j = 0; j < 3; j++) {
			if (fcolor[j]>255.f) fcolor[j] = 255.f;
			color[j] = (GLubyte) fcolor[j];
		}
		memcpy((GLubyte*)m_pPaintBitstart + i, color, 3);
	}
	RestoreContent();
	RestoreFront();
	refresh();
	m_pDoc->m_pUI->m_origView->refresh();
}

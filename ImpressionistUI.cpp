//
// impressionistUI.h
//
// The user interface part for the program.
//

#include <FL/fl_ask.h>

#include <math.h>

#include "impressionistUI.h"
#include "impressionistDoc.h"

/*
//------------------------------ Widget Examples -------------------------------------------------
Here is some example code for all of the widgets that you may need to add to the 
project.  You can copy and paste these into your code and then change them to 
make them look how you want.  Descriptions for all of the widgets here can be found 
in links on the fltk help session page.

//---------Window/Dialog and Menubar-----------------------------------
	
	//----To install a window--------------------------
	Fl_Window* myWindow = new Fl_Window(600, 300, "MyWindow");
		myWindow->user_data((void*)(this));	// record self to be used by static callback functions
		
		// install menu bar
		myMenubar = new Fl_Menu_Bar(0, 0, 600, 25);
		Fl_Menu_Item ImpressionistUI::myMenuItems[] = {
			{ "&File",		0, 0, 0, FL_SUBMENU },
				{ "&Load...",	FL_ALT + 'l', (Fl_Callback *)ImpressionistUI::cb_load },
				{ "&Save...",	FL_ALT + 's', (Fl_Callback *)ImpressionistUI::cb_save }.
				{ "&Quit",			FL_ALT + 'q', (Fl_Callback *)ImpressionistUI::cb_exit },
				{ 0 },
			{ "&Edit",		0, 0, 0, FL_SUBMENU },
				{ "&Copy",FL_ALT + 'c', (Fl_Callback *)ImpressionistUI::cb_copy, (void *)COPY },
				{ "&Cut",	FL_ALT + 'x', (Fl_Callback *)ImpressionistUI::cb_cut, (void *)CUT },
				{ "&Paste",	FL_ALT + 'v', (Fl_Callback *)ImpressionistUI::cb_paste, (void *)PASTE },
				{ 0 },
			{ "&Help",		0, 0, 0, FL_SUBMENU },
				{ "&About",	FL_ALT + 'a', (Fl_Callback *)ImpressionistUI::cb_about },
				{ 0 },
			{ 0 }
		};
		myMenubar->menu(myMenuItems);
    m_mainWindow->end();

	//----The window callback--------------------------
	// One of the callbacks
	void ImpressionistUI::cb_load(Fl_Menu_* o, void* v) 
	{	
		ImpressionistDoc *pDoc=whoami(o)->getDocument();

		char* newfile = fl_file_chooser("Open File?", "*.bmp", pDoc->getImageName() );
		if (newfile != NULL) {
			pDoc->loadImage(newfile);
		}
	}


//------------Slider---------------------------------------

	//----To install a slider--------------------------
	Fl_Value_Slider * mySlider = new Fl_Value_Slider(10, 80, 300, 20, "My Value");
	mySlider->user_data((void*)(this));	// record self to be used by static callback functions
	mySlider->type(FL_HOR_NICE_SLIDER);
    mySlider->labelfont(FL_COURIER);
    mySlider->labelsize(12);
	mySlider->minimum(1);
	mySlider->maximum(40);
	mySlider->step(1);
	mySlider->value(m_nMyValue);
	mySlider->align(FL_ALIGN_RIGHT);
	mySlider->callback(cb_MyValueSlides);

	//----The slider callback--------------------------
	void ImpressionistUI::cb_MyValueSlides(Fl_Widget* o, void* v)
	{
		((ImpressionistUI*)(o->user_data()))->m_nMyValue=int( ((Fl_Slider *)o)->value() ) ;
	}
	

//------------Choice---------------------------------------
	
	//----To install a choice--------------------------
	Fl_Choice * myChoice = new Fl_Choice(50,10,150,25,"&myChoiceLabel");
	myChoice->user_data((void*)(this));	 // record self to be used by static callback functions
	Fl_Menu_Item ImpressionistUI::myChoiceMenu[3+1] = {
	  {"one",FL_ALT+'p', (Fl_Callback *)ImpressionistUI::cb_myChoice, (void *)ONE},
	  {"two",FL_ALT+'l', (Fl_Callback *)ImpressionistUI::cb_myChoice, (void *)TWO},
	  {"three",FL_ALT+'c', (Fl_Callback *)ImpressionistUI::cb_myChoice, (void *)THREE},
	  {0}
	};
	myChoice->menu(myChoiceMenu);
	myChoice->callback(cb_myChoice);
	
	//-----The choice callback-------------------------
	void ImpressionistUI::cb_myChoice(Fl_Widget* o, void* v)
	{
		ImpressionistUI* pUI=((ImpressionistUI *)(o->user_data()));
		ImpressionistDoc* pDoc=pUI->getDocument();

		int type=(int)v;

		pDoc->setMyType(type);
	}


//------------Button---------------------------------------

	//---To install a button---------------------------
	Fl_Button* myButton = new Fl_Button(330,220,50,20,"&myButtonLabel");
	myButton->user_data((void*)(this));   // record self to be used by static callback functions
	myButton->callback(cb_myButton);

	//---The button callback---------------------------
	void ImpressionistUI::cb_myButton(Fl_Widget* o, void* v)
	{
		ImpressionistUI* pUI=((ImpressionistUI*)(o->user_data()));
		ImpressionistDoc* pDoc = pUI->getDocument();
		pDoc->startPainting();
	}


//---------Light Button------------------------------------
	
	//---To install a light button---------------------
	Fl_Light_Button* myLightButton = new Fl_Light_Button(240,10,150,25,"&myLightButtonLabel");
	myLightButton->user_data((void*)(this));   // record self to be used by static callback functions
	myLightButton->callback(cb_myLightButton);

	//---The light button callback---------------------
	void ImpressionistUI::cb_myLightButton(Fl_Widget* o, void* v)
	{
		ImpressionistUI *pUI=((ImpressionistUI*)(o->user_data()));

		if (pUI->myBool==TRUE) pUI->myBool=FALSE;
		else pUI->myBool=TRUE;
	}

//----------Int Input--------------------------------------

    //---To install an int input-----------------------
	Fl_Int_Input* myInput = new Fl_Int_Input(200, 50, 5, 5, "&My Input");
	myInput->user_data((void*)(this));   // record self to be used by static callback functions
	myInput->callback(cb_myInput);

	//---The int input callback------------------------
	void ImpressionistUI::cb_myInput(Fl_Widget* o, void* v)
	{
		((ImpressionistUI*)(o->user_data()))->m_nMyInputValue=int( ((Fl_Int_Input *)o)->value() );
	}

//------------------------------------------------------------------------------------------------
*/

//------------------------------------- Help Functions --------------------------------------------

//------------------------------------------------------------
// This returns the UI, given the menu item.  It provides a
// link from the menu items to the UI
//------------------------------------------------------------
ImpressionistUI* ImpressionistUI::whoami(Fl_Menu_* o)	
{
	return ( (ImpressionistUI*)(o->parent()->user_data()) );
}


//--------------------------------- Callback Functions --------------------------------------------

//------------------------------------------------------------------
// Brings up a file chooser and then loads the chosen image
// This is called by the UI when the load image menu item is chosen
//------------------------------------------------------------------
void ImpressionistUI::cb_load_image(Fl_Menu_* o, void* v) 
{
	ImpressionistDoc *pDoc=whoami(o)->getDocument();

	char* newfile = fl_file_chooser("Open File?", "*.bmp", pDoc->getImageName() );
	if (newfile != NULL) {
		pDoc->loadImage(newfile);
	}
}


//------------------------------------------------------------------
// Brings up a file chooser and then saves the painted image
// This is called by the UI when the save image menu item is chosen
//------------------------------------------------------------------
void ImpressionistUI::cb_save_image(Fl_Menu_* o, void* v) 
{
	ImpressionistDoc *pDoc=whoami(o)->getDocument();

	char* newfile = fl_file_chooser("Save File?", "*.bmp", "save.bmp" );
	if (newfile != NULL) {
		pDoc->saveImage(newfile);
	}
}

//-------------------------------------------------------------
// Brings up the paint dialog
// This is called by the UI when the brushes menu item
// is chosen
//-------------------------------------------------------------
void ImpressionistUI::cb_brushes(Fl_Menu_* o, void* v) 
{
	whoami(o)->m_brushDialog->show();
}

//------------------------------------------------------------
// Clears the paintview canvas.
// Called by the UI when the clear canvas menu item is chosen
//------------------------------------------------------------
void ImpressionistUI::cb_clear_canvas(Fl_Menu_* o, void* v)
{
	ImpressionistDoc* pDoc=whoami(o)->getDocument();

	pDoc->clearCanvas();
}

//------------------------------------------------------------
// Causes the Impressionist program to exit
// Called by the UI when the quit menu item is chosen
//------------------------------------------------------------
void ImpressionistUI::cb_exit(Fl_Menu_* o, void* v) 
{
	whoami(o)->m_mainWindow->hide();
	whoami(o)->m_brushDialog->hide();

}

// Brings up a filter dialog box
void ImpressionistUI::cb_filter(Fl_Menu_* o, void* v)
{
	whoami(o)->m_filerDialog->show();
}

//-----------------------------------------------------------
// Brings up an about dialog box
// Called by the UI when the about menu item is chosen
//-----------------------------------------------------------
void ImpressionistUI::cb_about(Fl_Menu_* o, void* v) 
{
	fl_message("Impressionist FLTK version for CS341, Spring 2002");
}

//-----------------------------------------------------------
// Undo the brush
//-----------------------------------------------------------
void ImpressionistUI::cb_undo(Fl_Menu_* o, void* v)
{
	whoami(o)->m_paintView->undo();
}

//-----------------------------------------------------------
// Swap content of the views
//-----------------------------------------------------------
void ImpressionistUI::cb_swap(Fl_Menu_* o, void* v)
{
	whoami(o)->swapViewContent();
}

//-----------------------------------------------------------
// Brings up a color dialog box
//-----------------------------------------------------------
void ImpressionistUI::cb_colors(Fl_Menu_* o, void* v)
{
	whoami(o)->m_colorDialog->show();
}


//-----------------------------------------------------------
// Turn the originalview into grayscale
//-----------------------------------------------------------
void ImpressionistUI::cb_grayscale(Fl_Menu_* o, void* v)
{
	ImpressionistDoc* pDoc = whoami(o)->getDocument();
	if (pDoc->m_ucBitmap == NULL) return; 
	int size = pDoc->m_nPaintHeight * pDoc->m_nPaintWidth * 3;
	pDoc->m_pUI->m_origView->grayscale(pDoc->m_ucGray);
	memcpy(pDoc->m_ucBitmap, pDoc->m_ucGray, size);
	pDoc->m_pUI->m_origView->refresh();
}


//-----------------------------------------------------------
// Load the base image to original view
//-----------------------------------------------------------
void ImpressionistUI::cb_displayOriginal(Fl_Menu_* o, void* v)
{
	whoami(o)->m_origView->reload();
}


void ImpressionistUI::toggleWidget(Fl_Widget* o, int type)
{
	ImpressionistUI* pUI = ((ImpressionistUI *)(o->user_data()));
	ImpressionistDoc* pDoc = pUI->getDocument();

	// Disable slider
	pUI->m_LineWidthSlider->deactivate();
	pUI->m_AngleSlider->deactivate();
	pUI->m_StrokeDirectionChoice->deactivate();
	pUI->m_paintView->setStroke(false);

	// Enable slider (line width, line angle)
	switch (type) {
	case BRUSH_LINES:
	case BRUSH_SCATTERED_LINES:
		pUI->m_LineWidthSlider->activate();
		pUI->m_AngleSlider->activate();
		pUI->m_StrokeDirectionChoice->activate();
		pUI->m_paintView->setStroke(true);
		break;
	default:
		break;
	}
}

//------- UI should keep track of the current for all the controls for answering the query from Doc ---------
//-------------------------------------------------------------
// Sets the type of brush to use to the one chosen in the brush 
// choice.  
// Called by the UI when a brush is chosen in the brush choice
//-------------------------------------------------------------
void ImpressionistUI::cb_brushChoice(Fl_Widget* o, void* v)
{
	ImpressionistUI* pUI=((ImpressionistUI *)(o->user_data()));
	ImpressionistDoc* pDoc=pUI->getDocument();

	int type=(int)v;

	toggleWidget(o, type);
	
	pDoc->setBrushType(type);
}

//-------------------------------------------------------------
// Sets the stroke direction to use to the one chosen in the stroke 
// driection menu.  
// Called by the UI when an option is chosen in the stroke direction
//-------------------------------------------------------------
void ImpressionistUI::cb_strokeDirection(Fl_Widget* o, void* v)
{
	ImpressionistUI* pUI = ((ImpressionistUI *)(o->user_data()));
	ImpressionistDoc* pDoc = pUI->getDocument();

	int opt = (int)v;

	pDoc->setStrokeDirection(opt);
}

//------------------------------------------------------------
// Clears the paintview canvas.
// Called by the UI when the clear canvas button is pushed
//------------------------------------------------------------
void ImpressionistUI::cb_clear_canvas_button(Fl_Widget* o, void* v)
{
	ImpressionistDoc * pDoc = ((ImpressionistUI*)(o->user_data()))->getDocument();

	pDoc->clearCanvas();
}


//-----------------------------------------------------------
// Updates the brush size to use from the value of the size
// slider
// Called by the UI when the size slider is moved
//-----------------------------------------------------------
void ImpressionistUI::cb_sizeSlides(Fl_Widget* o, void* v)
{
	((ImpressionistUI*)(o->user_data()))->m_nSize=int( ((Fl_Slider *)o)->value() ) ;
}

//-----------------------------------------------------------
// Updates the brush opacity to use from the value of the opacity
// slider
// Called by the UI when the opacity slider is moved
//-----------------------------------------------------------
void ImpressionistUI::cb_opacitySlides(Fl_Widget* o, void* v)
{
	((ImpressionistUI*)(o->user_data()))->m_nOpacity = float(((Fl_Slider *)o)->value());
}

//-----------------------------------------------------------
// Updates the line width to use from the value of the line width
// slider
// Called by the UI when the line width slider is moved
//-----------------------------------------------------------
void ImpressionistUI::cb_lineWidthSlides(Fl_Widget* o, void* v)
{
	((ImpressionistUI*)(o->user_data()))->m_nLineWidth = int(((Fl_Slider *)o)->value());
}

//-----------------------------------------------------------
// Updates the line angle to use from the value of the line angle
// slider
// Called by the UI when the line angle slider is moved
//-----------------------------------------------------------
void ImpressionistUI::cb_angleSlides(Fl_Widget* o, void* v)
{
	((ImpressionistUI*)(o->user_data()))->m_nAngle = int(((Fl_Slider *)o)->value());
}

//-----------------------------------------------------------
// Updates the spacing to use from the value of the spacing
// slider
// Called by the UI when the spacing slider is moved
//-----------------------------------------------------------
void ImpressionistUI::cb_spacingSlides(Fl_Widget* o, void* v)
{
	((ImpressionistUI*)(o->user_data()))->m_nSpacing = int(((Fl_Slider *)o)->value());
}

//---The size rand light button callback---------------------
void ImpressionistUI::cb_sizeRandBtn(Fl_Widget* o, void* v)
{
	ImpressionistUI *pUI = ((ImpressionistUI*)(o->user_data()));

	pUI->m_nSizeRand = !pUI->m_nSizeRand;
}

//---The normalize button callback---------------------
void ImpressionistUI::cb_normalizeBtn(Fl_Widget* o, void* v)
{
	ImpressionistUI *pUI = ((ImpressionistUI*)(o->user_data()));

	pUI->m_nNormalize = !pUI->m_nNormalize;
}

void ImpressionistUI::cb_paintBtn(Fl_Widget* o, void* v)
{
	ImpressionistUI *pUI = ((ImpressionistUI*)(o->user_data()));
	pUI->m_paintView->paintAll();
}

// when R slides
void ImpressionistUI::cb_rSlides(Fl_Widget* o, void* v)
{
	ImpressionistUI *pUI = ((ImpressionistUI*)(o->user_data()));
	pUI->m_nR = float(((Fl_Slider *)o)->value());
}

// when G slides
void ImpressionistUI::cb_gSlides(Fl_Widget* o, void* v)
{
	ImpressionistUI *pUI = ((ImpressionistUI*)(o->user_data()));
	pUI->m_nG = float(((Fl_Slider *)o)->value());
}

// when B slides
void ImpressionistUI::cb_bSlides(Fl_Widget* o, void* v)
{
	ImpressionistUI *pUI = ((ImpressionistUI*)(o->user_data()));
	pUI->m_nB = float(((Fl_Slider *)o)->value());
}

// apply color scaling to orignalview
void ImpressionistUI::cb_applyOriginal(Fl_Widget* o, void* v)
{
	ImpressionistUI *pUI = ((ImpressionistUI*)(o->user_data()));
	pUI->m_origView->scaleColor(pUI->m_nR, pUI->m_nG, pUI->m_nB);
}

// apply color scaling to paintview
void ImpressionistUI::cb_applyPaint(Fl_Widget* o, void* v)
{
	ImpressionistUI *pUI = ((ImpressionistUI*)(o->user_data()));
	pUI->m_paintView->scaleColor(pUI->m_nR, pUI->m_nG, pUI->m_nB);
}

// filter dialog, apply filter
void ImpressionistUI::cb_applyFilter(Fl_Widget* o, void* v)
{
	ImpressionistUI* pUI = ((ImpressionistUI*)(o->user_data()));
	ImpressionistDoc* pDoc = pUI->getDocument();

	if (pDoc->m_ucBitmap == NULL) return;
	int matrix[25];
	for (int i = 0; i < 25; i++) {
		matrix[i] = pUI->valInput[i]->value();
	}
	pDoc->ApplyFilter(pDoc->m_ucBitmap, pDoc->m_ucPainting, matrix, 5, pUI->m_nNormalize);
	pUI->m_paintView->refresh();
}


//---------------------------------- per instance functions --------------------------------------

//------------------------------------------------
// Return the ImpressionistDoc used
//------------------------------------------------
ImpressionistDoc* ImpressionistUI::getDocument()
{
	return m_pDoc;
}

// Swap content of Original View and Paint View
void ImpressionistUI::swapViewContent()
{
	if (!m_pDoc->m_ucBitmap && !m_pDoc->m_ucPainting) return;
	int size = m_pDoc->m_nPaintHeight*m_pDoc->m_nPaintWidth * 3;
	unsigned char* tmp = new unsigned char[size];
	memcpy(tmp, m_pDoc->m_ucPainting, size);
	memcpy(m_pDoc->m_ucPainting, m_pDoc->m_ucBitmap, size);
	memcpy(m_pDoc->m_ucBitmap, tmp, size);
	m_paintView->refresh();
	m_origView->refresh();
}

//------------------------------------------------
// Draw the main window
//------------------------------------------------
void ImpressionistUI::show() {
	m_mainWindow->show();
	m_paintView->show();
	m_origView->show();
}

//------------------------------------------------
// Change the paint and original window sizes to 
// w by h
//------------------------------------------------
void ImpressionistUI::resize_windows(int w, int h) {
	m_paintView->size(w,h);
	m_origView->size(w,h);
}

//------------------------------------------------ 
// Set the ImpressionistDoc used by the UI to 
// communicate with the brushes 
//------------------------------------------------
void ImpressionistUI::setDocument(ImpressionistDoc* doc)
{
	m_pDoc = doc;

	m_origView->m_pDoc = doc;
	m_paintView->m_pDoc = doc;
}

//------------------------------------------------
// Return the brush size
//------------------------------------------------
int ImpressionistUI::getSize()
{
	return m_nSize;
}

//-------------------------------------------------
// Set the brush size
//-------------------------------------------------
void ImpressionistUI::setSize( int size, bool updateSlider )
{
	m_nSize=size;

	if (size <= 40 && updateSlider)
		m_BrushSizeSlider->value(m_nSize);
}

//------------------------------------------------
// Return the opacity of brush
//------------------------------------------------
float ImpressionistUI::getOpacity()
{
	return m_nOpacity;
}

//-------------------------------------------------
// Set the opacity of brush
//-------------------------------------------------
void ImpressionistUI::setOpacity(float opacity)
{
	m_nOpacity = opacity;

	if (opacity <= 1.0)
		m_BrushOpacitySlider->value(m_nOpacity);
}

//------------------------------------------------
// Return the line width
//------------------------------------------------
int ImpressionistUI::getLineWidth()
{
	return m_nLineWidth;
}

//-------------------------------------------------
// Set the line width
//-------------------------------------------------
void ImpressionistUI::setLineWidth(int lineWidth)
{
	m_nLineWidth = lineWidth;

	if (lineWidth <= 40)
		m_LineWidthSlider->value(m_nLineWidth);
}

//------------------------------------------------
// Return the line angle (in degree)
//------------------------------------------------
int ImpressionistUI::getAngle()
{
	return m_nAngle;
}

//------------------------------------------------
// Return the spacing
//------------------------------------------------
int ImpressionistUI::getSpacing()
{
	return m_nSpacing;
}

//------------------------------------------------
// Return whether size random
//------------------------------------------------
bool ImpressionistUI::getSizeRand()
{
	return m_nSizeRand;
}

//-------------------------------------------------
// Set the line angle
//-------------------------------------------------
void ImpressionistUI::setAngle(int Angle)
{
	m_nAngle = Angle;

	if (Angle <= 359)
		m_AngleSlider->value(m_nAngle);
}

// Main menu definition
Fl_Menu_Item ImpressionistUI::menuitems[] = {
	{ "&File",		0, 0, 0, FL_SUBMENU },
		{ "&Load Image...",	FL_ALT + 'L', (Fl_Callback *)ImpressionistUI::cb_load_image },
		{ "&Save Image...",	FL_ALT + 'S', (Fl_Callback *)ImpressionistUI::cb_save_image },
		{ "&Brushes...",	FL_ALT + 'B', (Fl_Callback *)ImpressionistUI::cb_brushes }, 
		{ "&Clear Canvas", FL_ALT + 'C', (Fl_Callback *)ImpressionistUI::cb_clear_canvas, 0, FL_MENU_DIVIDER },
		
		{ "&Quit",			FL_ALT + 'Q', (Fl_Callback *)ImpressionistUI::cb_exit },
		{ 0 },

	{ "&Edit", 0, 0, 0, FL_SUBMENU },
		{ "&Undo", FL_CTRL + 'Z', (Fl_Callback *)ImpressionistUI::cb_undo },
		{ "&Colors", FL_CTRL + 'C', (Fl_Callback *)ImpressionistUI::cb_colors },
		{ "&Swap", FL_CTRL + 'W', (Fl_Callback *)ImpressionistUI::cb_swap },
		{ "&Filter", FL_CTRL + 'F', (Fl_Callback *)ImpressionistUI::cb_filter },
		{ 0 },

	{ "&Display", 0, 0, 0, FL_SUBMENU },
		{ "O&riginal Image", FL_CTRL + 'R', (Fl_Callback *)ImpressionistUI::cb_displayOriginal },
		{ "&Gray Scale", FL_CTRL + 'G', (Fl_Callback *)ImpressionistUI::cb_grayscale },
		{ 0 },

	{ "&Help",		0, 0, 0, FL_SUBMENU },
		{ "&About",	FL_ALT + 'A', (Fl_Callback *)ImpressionistUI::cb_about },
		{ 0 },

	{ 0 }
};

// Brush choice menu definition
Fl_Menu_Item ImpressionistUI::brushTypeMenu[NUM_BRUSH_TYPE+1] = {
  {"Points",			FL_ALT+'p', (Fl_Callback *)ImpressionistUI::cb_brushChoice, (void *)BRUSH_POINTS},
  {"Lines",				FL_ALT+'l', (Fl_Callback *)ImpressionistUI::cb_brushChoice, (void *)BRUSH_LINES},
  {"Circles",			FL_ALT+'c', (Fl_Callback *)ImpressionistUI::cb_brushChoice, (void *)BRUSH_CIRCLES},
  {"Scattered Points",	FL_ALT+'q', (Fl_Callback *)ImpressionistUI::cb_brushChoice, (void *)BRUSH_SCATTERED_POINTS},
  {"Scattered Lines",	FL_ALT+'m', (Fl_Callback *)ImpressionistUI::cb_brushChoice, (void *)BRUSH_SCATTERED_LINES},
  {"Scattered Circles",	FL_ALT+'d', (Fl_Callback *)ImpressionistUI::cb_brushChoice, (void *)BRUSH_SCATTERED_CIRCLES},
  {"Triangles",			FL_ALT+'1', (Fl_Callback *)ImpressionistUI::cb_brushChoice, (void *)BRUSH_TRIANGLES},
  {"Stars",				FL_ALT+'2', (Fl_Callback *)ImpressionistUI::cb_brushChoice, (void *)BRUSH_STARS },
  {"Thunders",			FL_ALT+'3', (Fl_Callback *)ImpressionistUI::cb_brushChoice, (void *)BRUSH_THUNDERS },
  {"Hearts",			FL_ALT+'4', (Fl_Callback *)ImpressionistUI::cb_brushChoice, (void *)BRUSH_HEARTS },
  {"Spirals",			FL_ALT+'5', (Fl_Callback *)ImpressionistUI::cb_brushChoice, (void *)BRUSH_SPIRALS },
  {"Targets",			FL_ALT+'6', (Fl_Callback *)ImpressionistUI::cb_brushChoice, (void *)BRUSH_TARGETS },
  {"Blur",				FL_ALT+'7', (Fl_Callback *)ImpressionistUI::cb_brushChoice, (void *)BRUSH_BLUR },
  {0}
};

// Stroke direction menu definition
Fl_Menu_Item ImpressionistUI::strokeDirectionMenu[4] = {
		{ "Slider/Right Mouse", FL_ALT + 's', (Fl_Callback *)ImpressionistUI::cb_strokeDirection, (void *)SLIDERMOUSE },
		{ "Gradient", FL_ALT + 'g', (Fl_Callback *)ImpressionistUI::cb_strokeDirection, (void *)GRADIENT },
		{ "Brush Direction", FL_ALT + 'b', (Fl_Callback *)ImpressionistUI::cb_strokeDirection, (void *)BRUSHDIRECTION },
		{ 0 }
};


//----------------------------------------------------
// Constructor.  Creates all of the widgets.
// Add new widgets here
//----------------------------------------------------
ImpressionistUI::ImpressionistUI() {
	// Create the main window
	m_mainWindow = new Fl_Window(600, 300, "Impressionist");
	m_mainWindow->user_data((void*)(this));	// record self to be used by static callback functions
	// install menu bar
	m_menubar = new Fl_Menu_Bar(0, 0, 600, 25);
	m_menubar->menu(menuitems);

	// Create a group that will hold two sub windows inside the main
	// window
	Fl_Group* group = new Fl_Group(0, 25, 600, 275);

	// install paint view window
	m_paintView = new PaintView(300, 25, 300, 275, "This is the paint view");//0jon
	m_paintView->box(FL_DOWN_FRAME);

	// install original view window
	m_origView = new OriginalView(0, 25, 300, 275, "This is the orig view");//300jon
	m_origView->box(FL_DOWN_FRAME);
	m_origView->deactivate();

	group->end();
	Fl_Group::current()->resizable(group);
	m_mainWindow->end();

	// init values

	m_nSize = 10;
	m_nOpacity = 1.0;
	m_nLineWidth = 1;
	m_nAngle = 0;
	m_nSpacing = 4;
	m_nSizeRand = true;
	m_nNormalize = true;
	m_nR = m_nG = m_nB = 1;

	// brush dialog definition
	m_brushDialog = new Fl_Window(400, 325, "Brush Dialog");

	// Add a brush type choice to the dialog
	m_BrushTypeChoice = new Fl_Choice(50, 8, 150, 25, "&Brush");
	m_BrushTypeChoice->user_data((void*)(this));	// record self to be used by static callback functions
	m_BrushTypeChoice->menu(brushTypeMenu);
	m_BrushTypeChoice->callback(cb_brushChoice);

	m_ClearCanvasButton = new Fl_Button(235, 8, 150, 25, "&Clear Canvas");
	m_ClearCanvasButton->user_data((void*)(this));
	m_ClearCanvasButton->callback(cb_clear_canvas_button);

	// Add a stroke direction choice to the dialog
	m_StrokeDirectionChoice = new Fl_Choice(112, 40, 150, 25, "&Stroke Direction");
	m_StrokeDirectionChoice->user_data((void*)(this));	// record self to be used by static callback functions
	m_StrokeDirectionChoice->menu(strokeDirectionMenu);
	m_StrokeDirectionChoice->callback(cb_strokeDirection);


	// Add brush size slider to the dialog 
	m_BrushSizeSlider = new Fl_Value_Slider(10, 75, 300, 20, "Size");
	m_BrushSizeSlider->user_data((void*)(this));	// record self to be used by static callback functions
	m_BrushSizeSlider->type(FL_HOR_NICE_SLIDER);
	m_BrushSizeSlider->labelfont(FL_COURIER);
	m_BrushSizeSlider->labelsize(12);
	m_BrushSizeSlider->minimum(1);
	m_BrushSizeSlider->maximum(40);
	m_BrushSizeSlider->step(1);
	m_BrushSizeSlider->value(m_nSize);
	m_BrushSizeSlider->align(FL_ALIGN_RIGHT);
	m_BrushSizeSlider->callback(cb_sizeSlides);

	// Add line width slider to the dialog 
	m_LineWidthSlider = new Fl_Value_Slider(10, 105, 300, 20, "Line Width");
	m_LineWidthSlider->user_data((void*)(this));	// record self to be used by static callback functions
	m_LineWidthSlider->type(FL_HOR_NICE_SLIDER);
	m_LineWidthSlider->labelfont(FL_COURIER);
	m_LineWidthSlider->labelsize(12);
	m_LineWidthSlider->minimum(1);
	m_LineWidthSlider->maximum(40);
	m_LineWidthSlider->step(1);
	m_LineWidthSlider->value(m_nLineWidth);
	m_LineWidthSlider->align(FL_ALIGN_RIGHT);
	m_LineWidthSlider->callback(cb_lineWidthSlides);

	// Add line angle slider to the dialog 
	m_AngleSlider = new Fl_Value_Slider(10, 135, 300, 20, "Line Angle");
	m_AngleSlider->user_data((void*)(this));	// record self to be used by static callback functions
	m_AngleSlider->type(FL_HOR_NICE_SLIDER);
	m_AngleSlider->labelfont(FL_COURIER);
	m_AngleSlider->labelsize(12);
	m_AngleSlider->minimum(0);
	m_AngleSlider->maximum(359);
	m_AngleSlider->step(1);
	m_AngleSlider->value(m_nAngle);
	m_AngleSlider->align(FL_ALIGN_RIGHT);
	m_AngleSlider->callback(cb_angleSlides);

	// Add brush opacity slider to the dialog 
	m_BrushOpacitySlider = new Fl_Value_Slider(10, 165, 300, 20, "Alpha");
	m_BrushOpacitySlider->user_data((void*)(this));	// record self to be used by static callback functions
	m_BrushOpacitySlider->type(FL_HOR_NICE_SLIDER);
	m_BrushOpacitySlider->labelfont(FL_COURIER);
	m_BrushOpacitySlider->labelsize(12);
	m_BrushOpacitySlider->minimum(0.0);
	m_BrushOpacitySlider->maximum(1.0);
	m_BrushOpacitySlider->step(0.01);
	m_BrushOpacitySlider->value(m_nOpacity);
	m_BrushOpacitySlider->align(FL_ALIGN_RIGHT);
	m_BrushOpacitySlider->callback(cb_opacitySlides);

	// add the auto paint options

	// spacing slider
	m_SpacingSlider = new Fl_Value_Slider(10, 200, 150, 25, "Spacing");
	m_SpacingSlider->user_data((void*)(this));	// record self to be used by static callback functions
	m_SpacingSlider->type(FL_HOR_NICE_SLIDER);
	m_SpacingSlider->labelfont(FL_COURIER);
	m_SpacingSlider->labelsize(12);
	m_SpacingSlider->minimum(1);
	m_SpacingSlider->maximum(16);
	m_SpacingSlider->step(1);
	m_SpacingSlider->value(m_nSpacing);
	m_SpacingSlider->align(FL_ALIGN_RIGHT);
	m_SpacingSlider->callback(cb_spacingSlides);

	// size random light button
	m_SizeRandBtn = new Fl_Light_Button(220, 200, 100, 25, "&Size Rand.");
	m_SizeRandBtn->user_data((void*)(this));   // record self to be used by static callback functions
	m_SizeRandBtn->value(m_nSizeRand);
	m_SizeRandBtn->callback(cb_sizeRandBtn);

	// paint button
	m_PaintButton = new Fl_Button(330, 200, 50, 25, "&Paint");
	m_PaintButton->user_data((void*)(this));
	m_PaintButton->callback(cb_paintBtn);

	// activate and deactivate sliders
	toggleWidget(m_BrushTypeChoice, 0);

	m_brushDialog->end();


	// Colors dialog
	m_colorDialog = new Fl_Window(400, 220, "Colors Dialog");

	// Add R, G, B sliders

	m_RSlider = new Fl_Value_Slider(25, 20, 300, 30, "Red");
	m_RSlider->user_data((void*)(this));	// record self to be used by static callback functions
	m_RSlider->type(FL_HOR_NICE_SLIDER);
	m_RSlider->labelfont(FL_COURIER);
	m_RSlider->labelsize(12);
	m_RSlider->minimum(0.0);
	m_RSlider->maximum(2.0);
	m_RSlider->step(0.01);
	m_RSlider->value(m_nR);
	m_RSlider->align(FL_ALIGN_RIGHT);
	m_RSlider->callback(cb_rSlides);

	m_GSlider = new Fl_Value_Slider(25, 60, 300, 30, "Green");
	m_GSlider->user_data((void*)(this));	// record self to be used by static callback functions
	m_GSlider->type(FL_HOR_NICE_SLIDER);
	m_GSlider->labelfont(FL_COURIER);
	m_GSlider->labelsize(12);
	m_GSlider->minimum(0.0);
	m_GSlider->maximum(2.0);
	m_GSlider->step(0.01);
	m_GSlider->value(m_nG);
	m_GSlider->align(FL_ALIGN_RIGHT);
	m_GSlider->callback(cb_gSlides);

	m_BSlider = new Fl_Value_Slider(25, 100, 300, 30, "Blue");
	m_BSlider->user_data((void*)(this));	// record self to be used by static callback functions
	m_BSlider->type(FL_HOR_NICE_SLIDER);
	m_BSlider->labelfont(FL_COURIER);
	m_BSlider->labelsize(12);
	m_BSlider->minimum(0.0);
	m_BSlider->maximum(2.0);
	m_BSlider->step(0.01);
	m_BSlider->value(m_nB);
	m_BSlider->align(FL_ALIGN_RIGHT);
	m_BSlider->callback(cb_bSlides);

	// apply buttons (to orginalview and paintview)

	m_ApplyOriginal = new Fl_Button(30, 150, 140, 30, "Apply to Original");
	m_ApplyOriginal->user_data((void*)(this));
	m_ApplyOriginal->callback(cb_applyOriginal);

	m_ApplyPaint = new Fl_Button(190, 150, 140, 30, "Apply to Paint");
	m_ApplyPaint->user_data((void*)(this));
	m_ApplyPaint->callback(cb_applyPaint);

	m_colorDialog->end();


	// filter dialog
	m_filerDialog = new Fl_Window(310, 310, "Filter Dialog");

	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < 5; j++) {
			valInput[i * 5 + j] = new Fl_Value_Input(20 + j * 55, 15 + i * 50, 40, 30, "");
			valInput[i * 5 + j]->value(0);
		}
	}

	// normalize light button
	m_NormalizeBtn = new Fl_Light_Button(100, 260, 100, 25, "Normalize");
	m_NormalizeBtn->user_data((void*)(this));
	m_NormalizeBtn->value(m_nNormalize);
	m_NormalizeBtn->callback(cb_normalizeBtn);

	m_ApplyFilter = new Fl_Button(205, 260, 80, 25, "Apply");
	m_ApplyFilter->user_data((void*)(this));
	m_ApplyFilter->callback(cb_applyFilter);

	m_filerDialog->end();

}
//
// impressionistUI.h
//
// The header file for the UI part
//

#ifndef ImpressionistUI_h
#define ImpressionistUI_h

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/fl_file_chooser.H>		// FLTK file chooser
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_Value_Slider.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Light_Button.H>
#include <FL/Fl_Value_Input.H>

#include "Impressionist.h"
#include "OriginalView.h"
#include "PaintView.h"

#include "ImpBrush.h"

class ImpressionistUI {
public:
	ImpressionistUI();

	// The FLTK widgets
	Fl_Window*			m_mainWindow;
	Fl_Menu_Bar*		m_menubar;
								
	PaintView*			m_paintView;
	OriginalView*		m_origView;

	// dialogs
	Fl_Window*			m_brushDialog;
	Fl_Window*			m_colorDialog;
	Fl_Window*			m_filerDialog;
	Fl_Choice*			m_BrushTypeChoice;
	Fl_Choice*			m_StrokeDirectionChoice;

	Fl_Value_Input*		valInput[25];

	Fl_Slider*			m_BrushSizeSlider;
	Fl_Slider*			m_BrushOpacitySlider;
	Fl_Slider*			m_LineWidthSlider;
	Fl_Slider*			m_AngleSlider;
	Fl_Slider*			m_SpacingSlider;
	Fl_Slider*			m_RSlider;
	Fl_Slider*			m_GSlider;
	Fl_Slider*			m_BSlider;

	Fl_Light_Button*	m_SizeRandBtn;
	Fl_Light_Button*	m_NormalizeBtn;

	Fl_Button*			m_PaintButton;
	Fl_Button*          m_ClearCanvasButton;
	Fl_Button*			m_ApplyOriginal;
	Fl_Button*			m_ApplyPaint;
	Fl_Button*			m_ApplyFilter;

	// Member functions
	void				setDocument(ImpressionistDoc* doc);
	ImpressionistDoc*	getDocument();

	void				swapViewContent();

	void				show();
	void				resize_windows(int w, int h);

	// Interface to get attribute

	int					getSize();
	void				setSize(int size, bool updateSlider);
	float				getOpacity();
	void				setOpacity(float opacity);
	int					getLineWidth();
	void				setLineWidth(int lineWidth);
	int					getAngle();
	void				setAngle(int Angle);
	int					getSpacing();
	bool				getSizeRand();


private:
	ImpressionistDoc*	m_pDoc;		// pointer to document to communicate with the document

	// All attributes here
	int		m_nSize;
	float	m_nOpacity;
	int		m_nLineWidth;
	int		m_nAngle;
	int		m_nSpacing;
	bool	m_nSizeRand;
	bool	m_nNormalize;
	float	m_nR;
	float	m_nG;
	float	m_nB;

	// self-defined functions
	static void toggleWidget(Fl_Widget* o, int type);

	// Static class members
	static Fl_Menu_Item		menuitems[];
	static Fl_Menu_Item		brushTypeMenu[NUM_BRUSH_TYPE+1];
	static Fl_Menu_Item		strokeDirectionMenu[4];

	static ImpressionistUI*	whoami(Fl_Menu_* o);

	// All callbacks here.  Callbacks are declared static
	static void	cb_load_image(Fl_Menu_* o, void* v);
	static void	cb_save_image(Fl_Menu_* o, void* v);
	static void	cb_brushes(Fl_Menu_* o, void* v);
	static void	cb_clear_canvas(Fl_Menu_* o, void* v);
	static void	cb_exit(Fl_Menu_* o, void* v);
	static void	cb_about(Fl_Menu_* o, void* v);
	static void	cb_brushChoice(Fl_Widget* o, void* v);
	static void cb_strokeDirection(Fl_Widget* o, void* v);
	static void	cb_clear_canvas_button(Fl_Widget* o, void* v);
	static void	cb_sizeSlides(Fl_Widget* o, void* v);
	static void cb_opacitySlides(Fl_Widget* o, void* v);
	static void cb_lineWidthSlides(Fl_Widget* o, void* v);
	static void cb_angleSlides(Fl_Widget* o, void* v);
	static void cb_spacingSlides(Fl_Widget* o, void* v);
	static void cb_sizeRandBtn(Fl_Widget* o, void* v);
	static void cb_normalizeBtn(Fl_Widget* o, void* v);
	static void cb_paintBtn(Fl_Widget* o, void* v);
	static void cb_undo(Fl_Menu_* o, void* v);
	static void cb_swap(Fl_Menu_* o, void* v);
	static void cb_colors(Fl_Menu_* o, void* v);
	static void cb_rSlides(Fl_Widget* o, void* v);
	static void cb_gSlides(Fl_Widget* o, void* v);
	static void cb_bSlides(Fl_Widget* o, void* v);
	static void cb_applyOriginal(Fl_Widget* o, void* v);
	static void cb_applyPaint(Fl_Widget* o, void* v);
	static void cb_grayscale(Fl_Menu_* o, void* v);
	static void cb_displayOriginal(Fl_Menu_* o, void* v);
	static void	cb_filter(Fl_Menu_* o, void* v);
	static void cb_applyFilter(Fl_Widget* o, void* v);

};

#endif

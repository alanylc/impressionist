//
// ImpBrush.cpp
//
// The implementation of virtual brush. All the other brushes inherit from it.
//

#include "impressionistDoc.h"
#include "impressionistUI.h"
#include "ImpBrush.h"


// Static class member initializations
int			ImpBrush::c_nBrushCount	= 0;
ImpBrush**	ImpBrush::c_pBrushes	= NULL;

// EPSILON for float number comparison
//float ImpBrush::EPSILON = 0.0001f;

ImpBrush::ImpBrush(ImpressionistDoc*	pDoc, 
				   char*				name) :
					m_pDoc(pDoc), 
					m_pBrushName(name)
{
}

//---------------------------------------------------
// Return m_pDoc, which connects the UI and brushes
//---------------------------------------------------
ImpressionistDoc* ImpBrush::GetDocument(void)
{
	return m_pDoc;
}

//---------------------------------------------------
// Return the name of the current brush
//---------------------------------------------------
char* ImpBrush::BrushName(void)
{
	return m_pBrushName;
}

//----------------------------------------------------
// Set the color to paint with to the color at source,
// which is the coord at the original window to sample 
// the color from
//----------------------------------------------------
void ImpBrush::SetColor (const Point source)
{
	ImpressionistDoc* pDoc = GetDocument();

	GLubyte color[4];

	memcpy ( color, pDoc->GetOriginalPixel( source ), 3 );

	color[3] = static_cast<GLubyte>(pDoc->getOpacity()*255.f);

	glColor4ubv( color );

}

// helper function to get min point
Point ImpBrush::MinPoint()
{
	ImpressionistDoc* pDoc = GetDocument();
	int lower_y = pDoc->m_pUI->m_mainWindow->h() - pDoc->m_nPaintHeight - 25;
	if (lower_y < 0) lower_y = 0; // initially after loading an image, the lower_y may be negative by the formula
	return Point(0, lower_y);
}

// helper function to get max point
Point ImpBrush::MaxPoint()
{
	ImpressionistDoc* pDoc = GetDocument();
	//int upper_y = pDoc->m_pUI->m_mainWindow->h() - 25;
	int upper_y = MinPoint().y + pDoc->m_nPaintHeight;
	return Point(pDoc->m_nPaintWidth, upper_y);
}

// check if pt is valid, and store a validPt that is closer to pt within the boundary
bool ImpBrush::ValidPointi(Point pt, Point& validPt)
{
	bool valid = true;
	Point maxPt = MaxPoint();
	Point minPt = MinPoint();
	validPt = Point(pt.x, pt.y);
	if (pt.x < minPt.x) { validPt.x = minPt.x; valid = false; }
	else if (pt.x > maxPt.x) { validPt.x = maxPt.x;  valid = false;  }
	if (pt.y < minPt.y) { validPt.y = minPt.y; valid = false;  }
	else if (pt.y > maxPt.y) { validPt.y = maxPt.y; valid = false;  }
	return valid;
}

// check if pt is valid, and store a validPt that is closer to pt within the boundary
bool ImpBrush::ValidPointf(Pointf pt, Pointf& validPt) 
{
	bool valid = true;
	Point maxPt = MaxPoint();
	Point minPt = MinPoint();
	validPt = Pointf(pt.x, pt.y);
	if (pt.x - minPt.x < -EPSILON) { validPt.x = (float)minPt.x; valid = false; }
	if (pt.x - maxPt.x > EPSILON) { validPt.x = (float)maxPt.x; valid = false; }
	if (pt.y - minPt.y < -EPSILON) { validPt.y = (float)minPt.y; valid = false; }
	if (pt.y - maxPt.y > EPSILON) { validPt.y = (float)maxPt.y; valid = false; }
	return valid;
}

void ImpBrush::computeLinePoints(float angle, Pointf midpt, int width, Pointf& leftPt, Pointf& rightPt)
{
	leftPt = Pointf(midpt.x - (width / 2 * cos(angle)), midpt.y - (width / 2 * sin(angle)));
	rightPt = Pointf(midpt.x + (width / 2 * cos(angle)), midpt.y + (width / 2 * sin(angle)));
	if (leftPt.x > rightPt.x) {
		Point tmp = Point(rightPt.x, rightPt.y);
		rightPt = Pointf(leftPt.x, leftPt.y);
		leftPt = Pointf(tmp.x, tmp.y);
	}
	/*
	// compute leftPt and rightPt within the paint area
	Point maxPt = MaxPoint();
	Point minPt = MinPoint();
	Pointf validLeftPt, validRightPt;
	if (!ValidPointf(leftPt, validLeftPt))
	{
		Pointf oldleftPt(leftPt.x, leftPt.y);
		if (abs(oldleftPt.x - validLeftPt.x) > EPSILON)
		{
			leftPt.x = validLeftPt.x;
			leftPt.y = tan(angle) * (leftPt.x - midpt.x) + midpt.y;
		}
		if (abs(oldleftPt.y - validLeftPt.y) > EPSILON && !ValidPointf(leftPt, Pointf()))
		{
			leftPt.y = validLeftPt.y;
			leftPt.x = midpt.x + (leftPt.y - midpt.y) / tan(angle);
		}
	}
	if (!ValidPointf(rightPt, validRightPt))
	{
		Pointf oldrightPt(rightPt.x, rightPt.y);
		if (abs(oldrightPt.x - validRightPt.x) > EPSILON)
		{
			rightPt.x = validRightPt.x;
			rightPt.y = tan(angle) * (rightPt.x - midpt.x) + midpt.y;
		}
		if (abs(oldrightPt.y - validRightPt.y) > EPSILON && !ValidPointf(rightPt, Pointf()))
		{
			rightPt.y = validRightPt.y;
			rightPt.x = midpt.x + (rightPt.y - midpt.y) / tan(angle);
		}
	}
	*/
}

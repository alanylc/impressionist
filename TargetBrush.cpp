//
// TargetBrush.cpp
//
// The implementation of Target Brush. 
// 
//

#include "impressionistDoc.h"
#include "impressionistUI.h"
#include "TargetBrush.h"
#include <math.h>

extern float frand();

TargetBrush::TargetBrush(ImpressionistDoc* pDoc, char* name) :
ImpBrush(pDoc, name)
{
}

void TargetBrush::BrushBegin(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* dlg = pDoc->m_pUI;

	glPointSize(1.f);
	glLineWidth(1.f);

	BrushMove(source, target);
}

void TargetBrush::BrushMove(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* dlg = pDoc->m_pUI;

	if (pDoc == NULL) {
		printf("TargetBrush::BrushMove  document is NULL\n");
		return;
	}

	if (!ValidPointi(target, Point())){
		// allow start dragging from out of boundary

		//printf("Out of Boundary\n");
		//return;
	}

	int size = pDoc->getSize();
	
	float radius = size*2;

	glBegin(GL_LINE_LOOP);

	SetColor(source);

	for (int i = 0; i <= 360; i += 4) {
		float x = target.x + sin((float)i*PI / 180) * radius * 0.8;
		float y = target.y + cos((float)i*PI / 180) * radius * 0.8;
		//if (!ValidPointf(Pointf(x, y), Pointf())) { continue; }
		glVertex2f(x, y);
	}

	glEnd();

	glBegin(GL_LINE_LOOP);

	SetColor(source);

	for (int i = 0; i <= 360; i += 4) {
		float x = target.x + sin((float)i*PI / 180) * radius * 0.5;
		float y = target.y + cos((float)i*PI / 180) * radius * 0.5;
		//if (!ValidPointf(Pointf(x, y), Pointf())) { continue; }
		glVertex2f(x, y);
	}

	glEnd();

	glBegin(GL_LINES);

	glVertex2f(target.x - radius, target.y);
	glVertex2f(target.x - radius * 0.35, target.y);

	glVertex2f(target.x + radius * 0.35, target.y);
	glVertex2f(target.x + radius, target.y);

	glVertex2f(target.x, target.y - radius);
	glVertex2f(target.x, target.y - radius * 0.35);

	glVertex2f(target.x, target.y + radius * 0.35);
	glVertex2f(target.x, target.y + radius);

	SetColor(source);



	glEnd();
}

void TargetBrush::BrushEnd(const Point source, const Point target)
{
	// do nothing so far
}


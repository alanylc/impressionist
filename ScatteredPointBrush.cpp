//
// ScatteredPointBrush.cpp
//
// The implementation of Scattered Point Brush. 
// 
//

#include "impressionistDoc.h"
#include "impressionistUI.h"
#include "ScatteredPointBrush.h"

extern float frand();

ScatteredPointBrush::ScatteredPointBrush(ImpressionistDoc* pDoc, char* name) :
ImpBrush(pDoc, name)
{
}

void ScatteredPointBrush::BrushBegin(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* dlg = pDoc->m_pUI;

	glPointSize(1.f);

	BrushMove(source, target);
}

void ScatteredPointBrush::BrushMove(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* dlg = pDoc->m_pUI;

	if (pDoc == NULL) {
		printf("ScatteredPointBrush::BrushMove  document is NULL\n");
		return;
	}

	int size = pDoc->getSize();

	Point minPt = MinPoint();
	Point maxPt = MaxPoint();

	if (!ValidPointi(target, Point())) {
		// allow start dragging from out of boundary

		//printf("Out of Boundary\n");
		//return;
	}

	int count = (int) size * size / 4;
	if (count == 0) count = 1;

	for (int i = 0; i < count; i++) {

		Point pt(target.x + frand()*size - size / 2, target.y + frand()*size - size / 2);

		ValidPointi(pt, pt);

		Point shifted_source = Point(source.x + (pt.x - target.x), source.y + (pt.y - target.y));

		glBegin(GL_POINTS);
		SetColor(shifted_source);

		glVertex2d(pt.x, pt.y);

		glEnd();
	}
}

void ScatteredPointBrush::BrushEnd(const Point source, const Point target)
{
	// do nothing so far
}


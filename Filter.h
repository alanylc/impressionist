//
// Filter.h
//
// The header file for filter that do image processing
//

#ifndef FILTER_H
#define FILTER_H

#include <stdlib.h>

class Filter
{
public:
	Filter(const unsigned char* _image, const int** _matrix, const int _size);

private:
	unsigned char* image;		// souce image
	unsigned char* product;		// output image storage
	int** matrix;				// filter kernel (must be square matrix)
	int size;					// size of matrix
};

#endif
//
// SpiralBrush.cpp
//
// The implementation of Sprial Brush. 
// 
//

#include "impressionistDoc.h"
#include "impressionistUI.h"
#include "SpiralBrush.h"
#include <cmath>

extern float frand();

SpiralBrush::SpiralBrush(ImpressionistDoc* pDoc, char* name) :
ImpBrush(pDoc, name)
{
}

void SpiralBrush::BrushBegin(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* dlg = pDoc->m_pUI;

	glPointSize(1.f);

	BrushMove(source, target);
}

void SpiralBrush::BrushMove(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* dlg = pDoc->m_pUI;

	if (pDoc == NULL) {
		printf("SpiralBrush::BrushMove  document is NULL\n");
		return;
	}

	Point minPt = MinPoint();
	Point maxPt = MaxPoint();
	if (!ValidPointi(target, Point())) {
		// allow start dragging from out of boundary

		//printf("Out of Boundary\n");
		//return;
	}

	int size = pDoc->getSize();

	glBegin(GL_LINE_STRIP);
	SetColor(source);

	for (int i = 0; i <= 360; i += 2) {
		float t = (float)i*PI / 180;
		float r = size * sqrt(t);
		float x = target.x + r * cos(t);
		float y = target.y + r * sin(t);
		//if (!ValidPointf(Pointf(x, y), Pointf())) { continue; }
		glVertex2f(x, y);
	}

	glEnd();

	glBegin(GL_LINE_STRIP);
	SetColor(source);

	for (int i = 0; i <= 360; i += 2) {
		float t = (float)i*PI / 180;
		float r = -size * sqrt(t);
		float x = target.x + r * cos(t);
		float y = target.y + r * sin(t);
		//if (!ValidPointf(Pointf(x, y), Pointf())) { continue; }
		glVertex2f(x, y);
	}

	glEnd();
}

void SpiralBrush::BrushEnd(const Point source, const Point target)
{
	// do nothing so far
}


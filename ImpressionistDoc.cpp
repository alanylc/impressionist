// 
// impressionistDoc.cpp
//
// It basically maintain the bitmap for answering the color query from the brush.
// It also acts as the bridge between brushes and UI (including views)
//

#include <FL/fl_ask.H>

#include "impressionistDoc.h"
#include "impressionistUI.h"

#include "ImpBrush.h"

// Include individual brush headers here.
#include "PointBrush.h"
#include "LineBrush.h"
#include "CircleBrush.h"
#include "ScatteredPointBrush.h"
#include "ScatteredLineBrush.h"
#include "ScatteredCircleBrush.h"
#include "TriangleBrush.h"
#include "TargetBrush.h"
#include "ThunderBrush.h"
#include "HeartBrush.h"
#include "SpiralBrush.h"
#include "StarBrush.h"
#include "BlurBrush.h"


#define DESTROY(p)	{  if ((p)!=NULL) {delete [] p; p=NULL; } }

ImpressionistDoc::ImpressionistDoc() 
{
	// Set NULL image name as init. 
	m_imageName[0]	='\0';	

	m_nWidth		= -1;
	m_ucBitmap		= NULL;
	m_ucPainting	= NULL;
	m_ucUndo		= NULL;
	m_ucBaseImage	= NULL;
	m_ucBlur		= NULL;
	m_ucColorBlur	= NULL;
	m_ucGray		= NULL;


	// create one instance of each brush
	ImpBrush::c_nBrushCount	= NUM_BRUSH_TYPE;
	ImpBrush::c_pBrushes	= new ImpBrush* [ImpBrush::c_nBrushCount];

	ImpBrush::c_pBrushes[BRUSH_POINTS]	= new PointBrush( this, "Points" );

	ImpBrush::c_pBrushes[BRUSH_LINES]				
		= new LineBrush( this, "Lines" );
	ImpBrush::c_pBrushes[BRUSH_CIRCLES]				
		= new CircleBrush( this, "Circles" );
	ImpBrush::c_pBrushes[BRUSH_SCATTERED_POINTS]	
		= new ScatteredPointBrush( this, "Scattered Points" );
	ImpBrush::c_pBrushes[BRUSH_SCATTERED_LINES]		
		= new ScatteredLineBrush(this, "Scattered Lines");
	ImpBrush::c_pBrushes[BRUSH_SCATTERED_CIRCLES]	
		= new ScatteredCircleBrush(this, "Scattered Circles");
	ImpBrush::c_pBrushes[BRUSH_TRIANGLES]
		= new TriangleBrush(this, "Triangles");
	ImpBrush::c_pBrushes[BRUSH_STARS]
		= new StarBrush(this, "Stars");
	ImpBrush::c_pBrushes[BRUSH_THUNDERS]
		= new ThunderBrush(this, "Thunders");
	ImpBrush::c_pBrushes[BRUSH_HEARTS]
		= new HeartBrush(this, "Hearts");
	ImpBrush::c_pBrushes[BRUSH_SPIRALS]
		= new SpiralBrush(this, "Spirals");
	ImpBrush::c_pBrushes[BRUSH_TARGETS]
		= new TargetBrush(this, "Targets");
	ImpBrush::c_pBrushes[BRUSH_BLUR]
		= new BlurBrush(this, "Blur");

	// make one of the brushes current
	m_pCurrentBrush	= ImpBrush::c_pBrushes[0];

	// make slider/right mouse be the default stroke direction
	m_nStrokeDirection = SLIDERMOUSE;

}


//---------------------------------------------------------
// Set the current UI 
//---------------------------------------------------------
void ImpressionistDoc::setUI(ImpressionistUI* ui) 
{
	m_pUI	= ui;
}

//---------------------------------------------------------
// Returns the active picture/painting name
//---------------------------------------------------------
char* ImpressionistDoc::getImageName() 
{
	return m_imageName;
}

//---------------------------------------------------------
// Called by the UI when the user changes the brush type.
// type: one of the defined brush types.
//---------------------------------------------------------
void ImpressionistDoc::setBrushType(int type)
{
	m_pCurrentBrush	= ImpBrush::c_pBrushes[type];
}

//---------------------------------------------------------
// Called by the UI when the user changes the stroke direction.
// type: one of the defined stroke direction options.
//---------------------------------------------------------
void ImpressionistDoc::setStrokeDirection(int opt)
{
	m_nStrokeDirection = opt;
}

//---------------------------------------------------------
// Returns the stroke direction
//---------------------------------------------------------
int ImpressionistDoc::getStrokeDirection()
{
	return m_nStrokeDirection;
}

//---------------------------------------------------------
// Returns the size of the brush.
//---------------------------------------------------------
int ImpressionistDoc::getSize()
{
	return m_pUI->getSize();
}

//---------------------------------------------------------
// Returns the opacity from slider.
//---------------------------------------------------------
float ImpressionistDoc::getOpacity()
{
	return m_pUI->getOpacity();
}

//---------------------------------------------------------
// Returns the line width from slider.
//---------------------------------------------------------
int ImpressionistDoc::getLineWidth()
{
	return m_pUI->getLineWidth();
}

//---------------------------------------------------------
// Returns the line angle from slider. (in degree)
//---------------------------------------------------------
int ImpressionistDoc::getAngle()
{
	return m_pUI->getAngle();
}

//---------------------------------------------------------
// Load the specified image
// This is called by the UI when the load image button is 
// pressed.
//---------------------------------------------------------
int ImpressionistDoc::loadImage(char *iname) 
{
	// try to open the image to read
	unsigned char*	data;
	int				width, 
					height;

	if ( (data=readBMP(iname, width, height))==NULL ) 
	{
		fl_alert("Can't load bitmap file");
		return 0;
	}

	// set the imageChanged flag to be true (turned off in original view - getGradient function)
	m_pUI->m_origView->imageChanged = true;

	// reflect the fact of loading the new image
	m_nWidth		= width;
	m_nPaintWidth	= width;
	m_nHeight		= height;
	m_nPaintHeight	= height;

	// release old storage
	if (m_ucBitmap) delete [] m_ucBitmap;
	if (m_ucPainting) delete [] m_ucPainting;
	if (m_ucUndo)	delete[] m_ucUndo;
	if (m_ucBaseImage) delete[] m_ucBaseImage;
	if (m_ucGray) delete[] m_ucGray;
	if (m_ucBlur) delete[] m_ucBlur;
	if (m_ucColorBlur) delete[] m_ucColorBlur;

	// save the base image
	m_ucBitmap = data;
	m_ucBaseImage = new unsigned char[width*height * 3];
	memcpy(m_ucBaseImage, m_ucBitmap, width*height * 3);

	// allocate space for draw view
	m_ucPainting	= new unsigned char [width*height*3];
	memset(m_ucPainting, 0, width*height*3);

	// allocate space for undo
	m_ucUndo = new unsigned char[width*height * 3];
	memset(m_ucUndo, 0, width*height * 3);

	m_pUI->m_mainWindow->resize(m_pUI->m_mainWindow->x(), 
								m_pUI->m_mainWindow->y(), 
								width*2, 
								height+25);

	// display it on origView
	m_pUI->m_origView->resizeWindow(width, height);	
	m_pUI->m_origView->refresh();

	// refresh paint view as well
	m_pUI->m_paintView->resizeWindow(width, height);	
	m_pUI->m_paintView->refresh();

	// allocate space for gray, blur, colorBlur image
	m_ucGray = new unsigned char[width*height * 3];
	memset(m_ucGray, 0, width*height * 3);
	m_ucBlur = new unsigned char[width*height * 3];
	memset(m_ucBlur, 0, width*height * 3);
	m_ucColorBlur = new unsigned char[width*height * 3];
	memset(m_ucColorBlur, 0, width*height * 3);

	return 1;
}


//----------------------------------------------------------------
// Save the specified image
// This is called by the UI when the save image menu button is 
// pressed.
//----------------------------------------------------------------
int ImpressionistDoc::saveImage(char *iname) 
{

	writeBMP(iname, m_nPaintWidth, m_nPaintHeight, m_ucPainting);

	return 1;
}

//----------------------------------------------------------------
// Clear the drawing canvas
// This is called by the UI when the clear canvas menu item is 
// chosen
//-----------------------------------------------------------------
int ImpressionistDoc::clearCanvas() 
{

	// Release old storage
	if ( m_ucPainting ) 
	{
		delete [] m_ucPainting;

		// allocate space for draw view
		m_ucPainting	= new unsigned char [m_nPaintWidth*m_nPaintHeight*3];
		memset(m_ucPainting, 0, m_nPaintWidth*m_nPaintHeight*3);

		// refresh paint view as well	
		m_pUI->m_paintView->refresh();
	}
	
	return 0;
}

//------------------------------------------------------------------
// Get the color of the pixel in the original image at coord x and y
//------------------------------------------------------------------
GLubyte* ImpressionistDoc::GetOriginalPixel( int x, int y )
{
	if ( x < 0 ) 
		x = 0;
	else if ( x >= m_nWidth ) 
		x = m_nWidth-1;

	if ( y < 0 ) 
		y = 0;
	else if ( y >= m_nHeight ) 
		y = m_nHeight-1;

	return (GLubyte*)(m_ucBitmap + 3 * (y*m_nWidth + x));
}

//----------------------------------------------------------------
// Get the color of the pixel in the original image at point p
//----------------------------------------------------------------
GLubyte* ImpressionistDoc::GetOriginalPixel( const Point p )
{
	return GetOriginalPixel( p.x, p.y );
}

// Get the color of input image at point p
GLubyte* ImpressionistDoc::GetPixel(const unsigned char* image, const Point p)
{
	int x = p.x;
	int y = p.y;
	if (x < 0)
		x = 0;
	else if (x >= m_nWidth)
		x = m_nWidth - 1;
	if (y < 0)
		y = 0;
	else if (y >= m_nHeight)
		y = m_nHeight - 1;
	return (GLubyte*)(image + 3 * (y*m_nWidth + x));
}

// Return the image after applying filter
void ImpressionistDoc::ApplyFilter(const unsigned char* image, unsigned char* output, const int* matrix, const int size, bool normalize)
{
	int totalWeight = 0;
	for (int a = 0; a < size; a++) {
		for (int b = 0; b < size; b++) {
			totalWeight += (matrix[a*size+b]);
		}
	}
	if (totalWeight == 0) totalWeight = 1;
	for (int h = 0; h < m_nPaintHeight; h++){
		for (int w = 0; w < m_nPaintWidth; w++){
			// final color init to zero
			double fcolor[3];
			fcolor[0] = 0.f;
			fcolor[1] = 0.f;
			fcolor[2] = 0.f;
			// compute sum of matrix before division
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < size; j++) {
					int hpos = (h - i + size / 2);
					if (hpos < 0) { hpos = 0; }
					else if (hpos >= m_nPaintHeight) { hpos = m_nPaintHeight - 1; }
					int wpos = (w - j + size / 2);
					if (wpos < 0) { wpos = 0; }
					else if (wpos >= m_nPaintWidth) { wpos = m_nPaintWidth - 1; }
					int pos = 3 * (hpos*m_nPaintWidth + wpos);
					GLubyte color[3];
					memcpy(color, (GLubyte*)(image + pos), 3);
					// add to final color
					for (int c = 0; c < 3; c++) {
						// Normalization
						if (normalize) {
							fcolor[c] += (double(color[c]) * matrix[i*size + j]) / totalWeight;
						}
						else {
							fcolor[c] += (double(color[c]) * matrix[i*size + j]);
						}
					}
				}
			}
			// RGB
			for (int c = 0; c < 3; c++) {
				// boundary checking
				if (fcolor[c] > 255.f) {
					fcolor[c] = 255.f;
				}
				else if (fcolor[c] < 0.f) {
					fcolor[c] = 0.f;
				}
			}
			GLubyte filtered[3];
			filtered[0] = (GLubyte)fcolor[0];
			filtered[1] = (GLubyte)fcolor[1];
			filtered[2] = (GLubyte)fcolor[2];
			memcpy(output + 3 * (h*m_nPaintWidth + w), filtered, 3);
		}
	}
}
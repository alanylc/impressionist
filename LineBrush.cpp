//
// LineBrush.cpp
//
// The implementation of Line Brush. 
// 
//

#include "impressionistDoc.h"
#include "impressionistUI.h"
#include "linebrush.h"
#include <cmath>

extern float frand();

LineBrush::LineBrush(ImpressionistDoc* pDoc, char* name) :
ImpBrush(pDoc, name)
{
}

void LineBrush::BrushBegin(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* dlg = pDoc->m_pUI;

	int lineWidth = pDoc->getLineWidth();
	glLineWidth((float)lineWidth);

	prevPt = Point(target.x, target.y);

	BrushMove(source, target);
}

void LineBrush::BrushMove(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* dlg = pDoc->m_pUI;

	if (pDoc == NULL) {
		printf("LineBrush::BrushMove  document is NULL\n");
		return;
	}

	Point minPt = MinPoint();
	Point maxPt = MaxPoint();
	if (!ValidPointi(target, Point())) {
		// allow start dragging from out of boundary

		//printf("Out of Boundary\n");
		//return;
	}

	int size = pDoc->getSize();

	if (size < 2) { 
		return;
	}

	float angle = 0.f;
	int dx = target.x - prevPt.x;
	int dy = target.y - prevPt.y;
	
	switch (pDoc->getStrokeDirection()) {
		case SLIDERMOUSE:
			angle = pDoc->getAngle() * PI / 180; // in radian
			break;
		case GRADIENT:
			angle = pDoc->m_pUI->m_origView->getGradient(source);
			break;
		case BRUSHDIRECTION:
			if (abs(dx) > 1 || abs(dy) > 1) {
				angle = atan2(dy, dx); // in radian
				if (angle < EPSILON) angle += 2 * PI;
			}
			else {
				return;
			}
			break;
		default:
			break;
	}
	
	Pointf leftPt, rightPt;
	computeLinePoints(angle, Pointf((float)target.x, (float)target.y), size, leftPt, rightPt);

	glBegin(GL_LINE_STRIP);
	SetColor(source);

	glVertex2d(leftPt.x, leftPt.y);
	glVertex2d(rightPt.x, rightPt.y);

	glEnd();

	prevPt = Point(target.x, target.y);
}

void LineBrush::BrushEnd(const Point source, const Point target)
{
	// do nothing so far
}


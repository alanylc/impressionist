//
// ThunderBrush.cpp
//
// The implementation of Thunder Brush. 
// 
//

#include "impressionistDoc.h"
#include "impressionistUI.h"
#include "ThunderBrush.h"
#include <math.h>

extern float frand();

ThunderBrush::ThunderBrush(ImpressionistDoc* pDoc, char* name) :
ImpBrush(pDoc, name)
{
}

void ThunderBrush::BrushBegin(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* dlg = pDoc->m_pUI;

	glPointSize(1.f);
	glLineWidth(1.f);

	BrushMove(source, target);
}

void ThunderBrush::BrushMove(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* dlg = pDoc->m_pUI;

	if (pDoc == NULL) {
		printf("ThunderBrush::BrushMove  document is NULL\n");
		return;
	}

	if (!ValidPointi(target, Point())){
		// allow start dragging from out of boundary

		//printf("Out of Boundary\n");
		//return;
	}

	int size = pDoc->getSize();

	float maxY = target.y + size;
	float minY = target.y - size * 1.2;
	float maxX = target.x + size * 0.75;
	float minX = target.x - size * 0.75;
	float width = size * 0.75 * 2;
	float height = size * 2.2;

	glBegin(GL_LINE_LOOP);

	SetColor(source);

	glVertex2f(target.x, maxY);
	glVertex2f(target.x + width * 0.5, maxY);
	glVertex2f(target.x+ width * 0.05, target.y + height * 0.1);
	glVertex2f(maxX, target.y + height * 0.1);
	glVertex2f(minX, minY);
	glVertex2f(target.x - width * 0.05, target.y - height * 0.1);
	glVertex2f(minX, target.y - height * 0.1);

	glEnd();
}

void ThunderBrush::BrushEnd(const Point source, const Point target)
{
	// do nothing so far
}


#ifndef IMPBRUSH_H
#define IMPBRUSH_H

//
// ImpBrush.h
//
// The header file of virtual brush. All the other brushes inherit from it.
//

#include <stdlib.h>
#include <cmath>

#define PI M_PI
#define EPSILON 0.000001f


// Each brush type has an associated constant.
enum
{
	BRUSH_POINTS = 0,
	BRUSH_LINES,
	BRUSH_CIRCLES,
	BRUSH_SCATTERED_POINTS,
	BRUSH_SCATTERED_LINES,
	BRUSH_SCATTERED_CIRCLES,
	BRUSH_TRIANGLES,
	BRUSH_STARS,
	BRUSH_THUNDERS,
	BRUSH_HEARTS,
	BRUSH_SPIRALS,
	BRUSH_TARGETS,
	BRUSH_BLUR,
	NUM_BRUSH_TYPE // Make sure this stays at the end!
};

// Stroke direction
enum
{
	SLIDERMOUSE = 0,
	GRADIENT,
	BRUSHDIRECTION
};

class ImpressionistDoc; // Pre-declaring class

class Point 
{
public:
	Point() {};
	Point(int xx, int yy) { x = xx; y = yy; };

	int x, y;
};

class Pointf
{
public:
	Pointf() {};
	Pointf(float xx, float yy) { x = xx; y = yy; };

	float x, y;
};


class ImpBrush 
{
protected:
	ImpBrush::ImpBrush( ImpressionistDoc* pDoc = NULL, char* name = NULL );

public:
	// The implementation of your brush should realize these virtual functions
	virtual void BrushBegin( const Point source, const Point target ) = 0;
	virtual void BrushMove( const Point source, const Point target ) = 0;
	virtual void BrushEnd( const Point source, const Point target ) = 0;

	// according to the source image and the position, determine the draw color
	void SetColor( const Point source );

	// get Doc to communicate with it
	ImpressionistDoc* GetDocument( void );

	// Return the name of the brush (not used in this version).
	char* BrushName( void );

	Point MinPoint();
	Point MaxPoint();
	bool ValidPointi(Point pt, Point& validPt);
	bool ValidPointf(Pointf pt, Pointf& validPt);
	// Compute leftPt and rightPt of a line given midpt, angle (in radian), and width
	void computeLinePoints(float angle, Pointf midpt, int width, Pointf& leftPt, Pointf& rightPt);
	void autoPaint(int spacing, bool sizeRand);
	
	
	static int			c_nBrushCount;	// How many brushes we have,
	static ImpBrush**	c_pBrushes;		// and what they are.

private:
	ImpressionistDoc*	m_pDoc;

	// Brush's name (not used in this version).
	char*				m_pBrushName;
};

#endif
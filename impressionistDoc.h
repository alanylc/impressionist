// 
// impressionistDoc.h
//
// header file for Doc 
//

#ifndef ImpressionistDoc_h
#define ImpressionistDoc_h

#include "impressionist.h"
#include "bitmap.h"

const int BOX3[9] = { 1, 1, 1, 1, 1, 1, 1, 1, 1 };

const int BOX5[25] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };

const int GAUSSIAN3[9] = {	1,	2,	1,
							2,	4,	2,
							1,	2,	1
};

const int GAUSSIAN5[25] = { 1,	4,	7,	4,  1,
							4,	16,	26,	16, 4,
							7,	26,	41,	26, 7,
							4,	16,	26,	16,	4,
							1,	4,	7,	4,	1,
};

class ImpressionistUI;

class ImpressionistDoc 
{
public:
	ImpressionistDoc();

	void	setUI(ImpressionistUI* ui);		// Assign the UI to use

	int		loadImage(char *iname);			// called by the UI to load image
	int		saveImage(char *iname);			// called by the UI to save image


	int     clearCanvas();                  // called by the UI to clear the drawing canvas
	void	setBrushType(int type);			// called by the UI to set the brushType
	void	setStrokeDirection(int opt);	// called by the UI to set the strokeDirection
	int		getStrokeDirection();			// get the stroke direction
	int		getSize();						// get the brush size
	float	getOpacity();					// get the opacity of brush
	int		getLineWidth();					// get the line width
	int		getAngle();						// get the rotation angle
	char*	getImageName();					// get the current image name
	

// Attributes
public:
	// Dimensions of original window.
	int				m_nWidth, 
					m_nHeight;
	// Dimensions of the paint window.
	int				m_nPaintWidth, 
					m_nPaintHeight;	
	// Bitmaps for original image and painting.
	unsigned char*	m_ucBitmap;
	unsigned char*	m_ucPainting;
	
	// Bitmaps for undo
	unsigned char*	m_ucUndo;

	// Base Image (never change while m_ucBitmap is the working one)
	unsigned char*	m_ucBaseImage;

	// gray, blur image
	unsigned char*	m_ucGray;
	unsigned char*	m_ucBlur;
	unsigned char*	m_ucColorBlur;


	// The current active brush.
	ImpBrush*			m_pCurrentBrush;	
	// Size of the brush.
	int m_nSize;							

	// The stroke direction opt
	int m_nStrokeDirection;

	ImpressionistUI*	m_pUI;

	// whether new image loaded, used for getGradient update
	bool imageChanged;

// Operations
public:
	// Get the color of the original picture at the specified coord
	GLubyte* GetOriginalPixel( int x, int y );   
	// Get the color of the original picture at the specified point	
	GLubyte* GetOriginalPixel( const Point p );  

	// Return the image after applying filter
	void ApplyFilter(const unsigned char* image, unsigned char* output, const int* matrix, const int size, bool normalize);

	// Get the color of input image at point p
	GLubyte* GetPixel(const unsigned char* image, const Point p);

private:
	char			m_imageName[256];

};

extern void MessageBox(char *message);

#endif

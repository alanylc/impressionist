//
// HeartBrush.cpp
//
// The implementation of Heart Brush. 
// 
//

#include "impressionistDoc.h"
#include "impressionistUI.h"
#include "HeartBrush.h"
#include <cmath>

extern float frand();

HeartBrush::HeartBrush(ImpressionistDoc* pDoc, char* name) :
ImpBrush(pDoc, name)
{
}

void HeartBrush::BrushBegin(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* dlg = pDoc->m_pUI;

	glPointSize(1.f);

	BrushMove(source, target);
}

void HeartBrush::BrushMove(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* dlg = pDoc->m_pUI;

	if (pDoc == NULL) {
		printf("HeartBrush::BrushMove  document is NULL\n");
		return;
	}

	Point minPt = MinPoint();
	Point maxPt = MaxPoint();
	if (!ValidPointi(target, Point())) {
		// allow start dragging from out of boundary

		//printf("Out of Boundary\n");
		//return;
	}

	int size = pDoc->getSize();

	glBegin(GL_TRIANGLE_FAN);
	SetColor(source);

	glVertex2f(target.x, target.y);

	for (int i = 0; i <= 360; i += 2) {
		float t = (float)i * PI / 180;
		float x = target.x + 16 * pow(sin(t), 3) * size/8 ;
		float y = target.y + ( 13 * cos(t) - 5 * cos(2*t) - 2 * cos(3*t) - cos(4*t) ) * size/8;
		//if (!ValidPointf(Pointf(x, y), Pointf())) { continue; } 
		glVertex2f(x, y);
	}

	glEnd();
}

void HeartBrush::BrushEnd(const Point source, const Point target)
{
	// do nothing so far
}


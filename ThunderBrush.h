//
// ThunderBrush.h
//
// The header file for Thunder Brush. 
//

#ifndef THUNDERBRUSH_H
#define THUNDERBRUSH_H

#include "ImpBrush.h"

class ThunderBrush : public ImpBrush
{
public:
	ThunderBrush(ImpressionistDoc* pDoc = NULL, char* name = NULL);

	void BrushBegin(const Point source, const Point target);
	void BrushMove(const Point source, const Point target);
	void BrushEnd(const Point source, const Point target);
	char* BrushName(void);
};

#endif
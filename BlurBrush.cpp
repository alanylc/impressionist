//
// BlurBrush.cpp
//
// The implementation of Blur Brush. 
// 
//

#include "impressionistDoc.h"
#include "impressionistUI.h"
#include "BlurBrush.h"
#include <math.h>

extern float frand();

BlurBrush::BlurBrush(ImpressionistDoc* pDoc, char* name) :
ImpBrush(pDoc, name)
{
	firstTime = true;
}

void BlurBrush::BrushBegin(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* pUI = pDoc->m_pUI;

	glPointSize(1.f);

	bool update = true;
	if (pUI->m_paintView->doPaint) {
		if (!firstTime) {
			update = false;
		}
	}
	if (update) {
		pDoc->ApplyFilter(pDoc->m_ucPainting, pDoc->m_ucColorBlur, GAUSSIAN5, 5, true);
		firstTime = false;
	}

	BrushMove(source, target);
}

void BlurBrush::BrushMove(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* pUI = pDoc->m_pUI;

	if (pDoc == NULL) {
		printf("BlurBrush::BrushMove  document is NULL\n");
		return;
	}

	if (!ValidPointi(target, Point())){
		// allow start dragging from out of boundary

		//printf("Out of Boundary\n");
		//return;
	}

	int size = pDoc->getSize()+1;

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {

			Point pt(target.x + i - size / 2, target.y + j - size / 2);

			ValidPointi(pt, pt);

			Point shifted_source = Point(source.x + i - size / 2, source.y + j - size / 2);

			glBegin(GL_POINTS);

			GLubyte color[4];

			memcpy(color, pDoc->GetPixel(pDoc->m_ucColorBlur, shifted_source), 3);

			color[3] = static_cast<GLubyte>(pDoc->getOpacity()*255.f);

			glColor4ubv(color);

			glVertex2d(pt.x, pt.y);

			glEnd();

		}
	}
}

void BlurBrush::BrushEnd(const Point source, const Point target)
{
	// do nothing so far
}


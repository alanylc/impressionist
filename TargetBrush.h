//
// TargetBrush.h
//
// The header file for Target Brush. 
//

#ifndef TARGETBRUSH_H
#define TARGETBRUSH_H

#include "ImpBrush.h"

class TargetBrush : public ImpBrush
{
public:
	TargetBrush(ImpressionistDoc* pDoc = NULL, char* name = NULL);

	void BrushBegin(const Point source, const Point target);
	void BrushMove(const Point source, const Point target);
	void BrushEnd(const Point source, const Point target);
	char* BrushName(void);
};

#endif
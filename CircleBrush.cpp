//
// CircleBrush.cpp
//
// The implementation of Circle Brush. 
// 
//

#include "impressionistDoc.h"
#include "impressionistUI.h"
#include "Circlebrush.h"
#include <math.h>

extern float frand();

CircleBrush::CircleBrush(ImpressionistDoc* pDoc, char* name) :
ImpBrush(pDoc, name)
{
}

void CircleBrush::BrushBegin(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* dlg = pDoc->m_pUI;

	int size = pDoc->getSize();

	glPointSize((float)size);

	BrushMove(source, target);
}

void CircleBrush::BrushMove(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* dlg = pDoc->m_pUI;

	if (pDoc == NULL) {
		printf("CircleBrush::BrushMove  document is NULL\n");
		return;
	}

	if ( !ValidPointi(target, Point()) ){
		// allow start dragging from out of boundary

		//printf("Out of Boundary\n");
		//return;
	}

	int size = pDoc->getSize();
	if (size < 2) {
		ImpBrush::c_pBrushes[BRUSH_POINTS]->BrushMove(source, target);
		return;
	}
	float radius = (float) size / 2;

	glBegin(GL_TRIANGLE_FAN);

		SetColor(source);

		glVertex2i(target.x, target.y);

		for (int i = 0; i <= 360; i += 4) {
			float x = target.x + sin((float)i*PI/180) * radius;
			float y = target.y + cos((float)i*PI/180) * radius;
			if (!ValidPointf(Pointf(x, y), Pointf())) { continue; }
			glVertex2f(x, y);
		}

	glEnd();
}

void CircleBrush::BrushEnd(const Point source, const Point target)
{
	// do nothing so far
}


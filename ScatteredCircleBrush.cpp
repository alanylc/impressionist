//
// ScatteredCircleBrush.cpp
//
// The implementation of Scattered Circle Brush. 
// 
//

#include "impressionistDoc.h"
#include "impressionistUI.h"
#include "ScatteredCircleBrush.h"
#include <math.h>

extern float frand();
extern int irand(int imax);

ScatteredCircleBrush::ScatteredCircleBrush(ImpressionistDoc* pDoc, char* name) :
ImpBrush(pDoc, name)
{
}

void ScatteredCircleBrush::BrushBegin(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* dlg = pDoc->m_pUI;

	int size = pDoc->getSize();

	glPointSize((float)size);

	BrushMove(source, target);
}

void ScatteredCircleBrush::BrushMove(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* dlg = pDoc->m_pUI;

	if (pDoc == NULL) {
		printf("ScatteredCircleBrush::BrushMove  document is NULL\n");
		return;
	}

	if (!ValidPointi(target, Point())) {
		// allow start dragging from out of boundary

		//printf("Out of Boundary\n");
		//return;
	}

	int size = (float)pDoc->getSize();
	if (size < 2) {
		ImpBrush::c_pBrushes[BRUSH_POINTS]->BrushMove(source, target);
		return;
	}
	float radius = size / 2;

	int circleCount = irand(3) + 2;

	for (int i = 0; i < circleCount; i++)
	{
		int xShift = irand((int)size*0.9) - (int)size*0.9 / 2 + 1;
		int yShift = irand((int)size*0.9) - (int)size*0.9 / 2 + 1;
		Point pt(target.x + xShift, target.y + yShift);
		ValidPointi(pt, pt);

		Point shifted_source = Point(source.x + (pt.x - target.x), source.y + (pt.y - target.y));

		glBegin(GL_TRIANGLE_FAN);

		SetColor(shifted_source);

		glVertex2f(pt.x, pt.y);

		for (int i = 0; i <= 360; i += 4) {
			float x = pt.x + sin((float)i*PI / 180) * radius;
			float y = pt.y + cos((float)i*PI / 180) * radius;
			if (!ValidPointf(Pointf(x,y), Pointf())) { continue; }
			glVertex2f(x, y);
		}

		glEnd();
	}
}

void ScatteredCircleBrush::BrushEnd(const Point source, const Point target)
{
	// do nothing so far
}


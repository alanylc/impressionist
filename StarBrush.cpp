//
// StarBrush.cpp
//
// The implementation of Star Brush. 
// 
//

#include "impressionistDoc.h"
#include "impressionistUI.h"
#include "StarBrush.h"
#include <math.h>

extern float frand();

StarBrush::StarBrush(ImpressionistDoc* pDoc, char* name) :
ImpBrush(pDoc, name)
{
}

void StarBrush::BrushBegin(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* dlg = pDoc->m_pUI;

	glLineWidth(1.f);

	BrushMove(source, target);
}

void StarBrush::BrushMove(const Point source, const Point target)
{
	ImpressionistDoc* pDoc = GetDocument();
	ImpressionistUI* dlg = pDoc->m_pUI;

	if (pDoc == NULL) {
		printf("StarBrush::BrushMove  document is NULL\n");
		return;
	}

	if (!ValidPointi(target, Point())){
		// allow start dragging from out of boundary

		//printf("Out of Boundary\n");
		//return;
	}

	int size = pDoc->getSize()+2;
	float rinner = size * 2, router = size * 3;
	const int arms = 5;
	float angle = PI / arms;

	glBegin(GL_LINE_LOOP);

	SetColor(source);

	for (int i = 0; i < 2*arms ; i++) {
		float r;
		if (i % 2 == 0) r = router;
		else r = rinner;
		float x = target.x + cos(i*angle) * r;
		float y = target.y + sin(i*angle) * r;
		glVertex2f(x, y);
	}

	glEnd();
}

void StarBrush::BrushEnd(const Point source, const Point target)
{
	// do nothing so far
}


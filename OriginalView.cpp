//
// originalview.cpp
//
// The code maintaining the original view of the input images
//

#include "impressionist.h"
#include "impressionistDoc.h"
#include "originalview.h"
#include "paintview.h"

#ifndef WIN32
#define min(a, b)	( ( (a)<(b) ) ? (a) : (b) )
#endif

OriginalView::OriginalView(int			x, 
						   int			y, 
						   int			w, 
						   int			h, 
						   const char*	l)
							: Fl_Gl_Window(x,y,w,h,l)
{
	m_nWindowWidth	= w;
	m_nWindowHeight	= h;
	imageChanged = true;
}

void OriginalView::draw()
{
	if(!valid())
	{
		glClearColor(0.7f, 0.7f, 0.7f, 1.0);

		// We're only using 2-D, so turn off depth 
		glDisable( GL_DEPTH_TEST );

		// Tell openGL to read from the front buffer when capturing
		// out paint strokes 
		glReadBuffer( GL_FRONT );
		ortho();
	}

	glClear( GL_COLOR_BUFFER_BIT );

	if ( m_pDoc->m_ucBitmap ) 
	{
		// note that both OpenGL pixel storage and the Windows BMP format
		// store pixels left-to-right, BOTTOM-to-TOP!!  thus all the fiddling
		// around with startrow.

		m_nWindowWidth=w();
		m_nWindowHeight=h();

		int drawWidth, drawHeight;

		// we are not using a scrollable window, so ignore it
		Point scrollpos;	// = GetScrollPosition();
		scrollpos.x=scrollpos.y=0;

		drawWidth	= min( m_nWindowWidth, m_pDoc->m_nWidth );
		drawHeight	= min( m_nWindowHeight, m_pDoc->m_nHeight );

		int	startrow	= m_pDoc->m_nHeight - (scrollpos.y + drawHeight);
		if ( startrow < 0 ) 
			startrow = 0;


		bitstart = m_pDoc->m_ucBitmap + 3 * ((m_pDoc->m_nWidth * startrow) + scrollpos.x);

		// just copy image to GLwindow conceptually
		glRasterPos2i( 0, m_nWindowHeight - drawHeight );
		glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );
		glPixelStorei( GL_UNPACK_ROW_LENGTH, m_pDoc->m_nWidth );
		glDrawBuffer( GL_BACK );
		glDrawPixels( drawWidth, drawHeight, GL_RGB, GL_UNSIGNED_BYTE, bitstart );
	}
			
	glFlush();

	// add marker corresponding to cursor
	glPointSize(5.f);
	glBegin(GL_POINTS);
	glColor3f(1.f, 0, 0);
	glVertex2i(pt.x, pt.y);
	glEnd();
}

void OriginalView::refresh()
{
	redraw();
}

void OriginalView::resizeWindow(int	width, 
								int	height)
{
	resize(x(), y(), width, height);
}

void OriginalView::setCoord(int x, int y)
{
	pt.x = x;
	pt.y = y;
}

void OriginalView::scaleColor(float r, float g, float b)
{
	if (m_pDoc->m_ucBitmap == NULL) return;
	GLubyte color[3];
	for (int i = 0; i < m_pDoc->m_nPaintHeight * m_pDoc->m_nPaintWidth * 3; i += 3){
		memcpy(color, (GLubyte*)bitstart + i, 3);
		float fcolor[3];
		for (int j = 0; j < 3; j++) fcolor[j] = (float)color[j];
		fcolor[0] *= r;
		fcolor[1] *= g;
		fcolor[2] *= b;
		for (int j = 0; j < 3; j++) {
			if (fcolor[j]>255.f) fcolor[j] = 255.f;
			color[j] = (GLubyte)fcolor[j];
		}
		memcpy((GLubyte*)bitstart + i, color, 3);
	}

	refresh();
}

void OriginalView::grayscale(unsigned char* result)
{
	if (m_pDoc->m_ucBitmap == NULL) return;
	if (result) delete[] result;
	result = new unsigned char[m_pDoc->m_nPaintHeight * m_pDoc->m_nPaintWidth * 3];
	memset(result, 0, m_pDoc->m_nPaintHeight * m_pDoc->m_nPaintWidth * 3);
	GLubyte color[3];
	for (int i = 0; i < m_pDoc->m_nPaintHeight * m_pDoc->m_nPaintWidth * 3; i += 3){
		memcpy(color, (GLubyte*)bitstart + i, 3);
		float intensity = 0.299f * color[0] + 0.587 * color[1] + 0.144 * color[2];
		color[0] = color[1] = color[2] = intensity;
		memcpy((GLubyte*)result + i, color, 3);
	}
}

void OriginalView::reload()
{
	if (m_pDoc->m_ucBaseImage == NULL || m_pDoc->m_ucBitmap == NULL) return;
	memcpy(m_pDoc->m_ucBitmap, m_pDoc->m_ucBaseImage, m_pDoc->m_nPaintHeight * m_pDoc->m_nPaintWidth * 3);
	refresh();
}

float OriginalView::getGradient(Point pt)
{
	if (imageChanged) {
		grayscale(m_pDoc->m_ucGray);
		m_pDoc->ApplyFilter(m_pDoc->m_ucGray, m_pDoc->m_ucBlur, GAUSSIAN5, 5, true);
		imageChanged = false;
	}
	if (!m_pDoc->m_pCurrentBrush->ValidPointi(pt, Point())) return 0.f;
	

	const int n = 3;

	int xpoints = 0;
	GLubyte cx[n][3];
	for (int i = 0; i < n; i++) {
		if (pt.x - i + n / 2 >= 0 && pt.x - i + n / 2 < m_pDoc->m_nPaintWidth) {
			xpoints++;
		}
		memcpy(cx[n - 1 - i], m_pDoc->GetPixel(m_pDoc->m_ucBlur, Point(pt.x - i + n / 2, pt.y)), 3);
	}
	float dx = 0.f;
	for (int i = 1; i < n; i++) {
		dx += (float)cx[i][0] - (float)cx[i - 1][0];
	}
	dx = dx / (xpoints - 1);

	int ypoints = 0;
	GLubyte cy[n][3];
	for (int i = 0; i < n; i++) {
		if (pt.y - i + n / 2 >= 0 && pt.y - i + n / 2 < m_pDoc->m_nPaintHeight) {
			ypoints++;
		}
		memcpy(cy[n - 1 - i], m_pDoc->GetPixel(m_pDoc->m_ucBlur, Point(pt.x, pt.y - i + n / 2)), 3);
	}
	float dy = 0.f;
	for (int i = 1; i < n; i++) {
		dy += (float)cy[i][0] - (float)cy[i - 1][0];
	}
	dy = dy / (ypoints - 1);

	float angle = atan2(dy, dx)+PI/2;

	return angle;
}